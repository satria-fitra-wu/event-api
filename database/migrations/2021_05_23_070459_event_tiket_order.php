<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EventTiketOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('event_ticket_order', function (Blueprint $table) {
            $table->id("event_ticket_order_id");
            $table->bigInteger("event_id")->default(-99);
            $table->bigInteger("event_ticket_id")->default(-99);
            $table->boolean("user_id_as_buyer");
            $table->string('order_code', 255);
            $table->bigInteger("order_user_id")->default(-99);
            $table->string('name', 255);
            $table->string('email', 255);
            $table->string('instance', 255);
            $table->string('phone', 255);
            $table->text('address');
            $table->text('payment_method');
            $table->text('order_status');
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('event_location');
    }
}
