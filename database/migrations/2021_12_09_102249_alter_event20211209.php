<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEvent20211209 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event', function (Blueprint $table) {

            $table->date('date_from')->nullable(true)->change();
            $table->date('date_to')->nullable(true)->change();
            $table->time('time_from')->nullable(true)->change();
            $table->time('time_to')->nullable(true)->change();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
