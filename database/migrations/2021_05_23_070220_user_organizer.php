<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserOrganizer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_organizer', function (Blueprint $table) {
            $table->id("user_id");
            $table->bigInteger("tenant_id")->default(-99);
            $table->string('organizer_name', 50);
            $table->string('verify_status', 255);
            $table->string('identity_no', 50);
            $table->text('identity_path');
            $table->string('identity_mime', 50);
            $table->string('npwp_no', 50);
            $table->text('npwp_path');
            $table->string('npwp_mime', 50);
            $table->string('email', 255);
            $table->string('instance', 255);
            $table->string('phone', 255);
            $table->text('address');
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
