<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventCertificateParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_certificate_parameter', function (Blueprint $table) {
            $table->id('event_certificate_parameter_id');
            $table->unsignedBigInteger('event_id');
            $table->string('parameter_name', 100);
            $table->string('parameter_type', 50);
            $table->string('parameter_value', 1024);
            $table->string('flg_dynamic_val', 1)->default("Y");
            $table->string('default_value', 1024);
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_certificate_parameter');
    }
}
