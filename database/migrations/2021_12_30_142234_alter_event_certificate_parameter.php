<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEventCertificateParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event_certificate_parameter', function (Blueprint $table) {
            $table->bigInteger('position_x')->default(-99);
            $table->bigInteger('position_y')->default(-99);
            $table->bigInteger('width')->default(-99);
            $table->bigInteger('height')->default(-99);
            $table->string('align')->default("");
            $table->bigInteger('font_size')->default(-99);

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
