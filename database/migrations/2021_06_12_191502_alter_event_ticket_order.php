<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEventTicketOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event_ticket_order', function (Blueprint $table) {
            $table->bigInteger('line_no')->default(0);
            $table->double("order_price")->default(0);
            $table->string("order_status_code", 5)->default("");
            $table->text("document_path");
            $table->text("document_mime");
            $table->text("remark");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
