<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTicketOrderHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_ticket_order_history', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("event_ticket_order_id");
            $table->bigInteger("event_id")->default(-99);
            $table->bigInteger("event_ticket_id")->default(-99);
            $table->boolean("user_id_as_buyer");
            $table->string('order_code', 255);
            $table->bigInteger("order_user_id")->default(-99);
            $table->string('name', 255);
            $table->string('email', 255);
            $table->string('instance', 255);
            $table->string('phone', 255);
            $table->text('address');
            $table->text('payment_method');
            $table->text('order_status');
            $table->bigInteger('line_no')->default(0);
            $table->double("order_price")->default(0);
            $table->string("order_status_code", 5)->default("");
            $table->text("document_path");
            $table->text("document_mime");
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_ticket_order_history');
    }
}
