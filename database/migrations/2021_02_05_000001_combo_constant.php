<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ComboConstant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('combo_constant', function (Blueprint $table) {
            $table->id();
            $table->string('combo_constant_code',50)->unique('idx_combo_constant_01');
            $table->string('combo_constant_name',100);
            $table->string('flg_system',1)->default("N");
            $table->integer('create_user_id')->default(-99);
            $table->integer('update_user_id')->default(-99);
            $table->integer('version')->default(0);
            $table->string('active',1)->default('');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('combo_constant');
    }
}
