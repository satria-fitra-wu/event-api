<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ComboConstantItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('combo_constant_item', function (Blueprint $table) {
            $table->id();
            $table->integer("combo_constant_id");
            $table->string('combo_constant_item_code',50);
            $table->string('combo_constant_item_name',100);
            $table->integer('create_user_id')->default(-99);
            $table->integer('update_user_id')->default(-99);
            $table->string('flg_system',1)->default("N");
            $table->integer('version')->default(0);
            $table->string('active',1)->default('');
            $table->timestamps();
            $table->unique(["combo_constant_id", "combo_constant_item_code"],"idx_combo_constant_item_01");
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('combo_constant_item');
    }
}
