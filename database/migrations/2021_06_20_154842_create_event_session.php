<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_session', function (Blueprint $table) {
            $table->id("event_session_id");
            $table->bigInteger("event_id")->default(-99);
            $table->bigInteger("line_no")->default(-99);
            $table->string('session_name', 255);
            $table->text('session_description');
            $table->bigInteger('event_ticket_id');
            $table->text('file_path');
            $table->string('file_mime', 50);
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_session');
    }
}
