<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailableRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_role', function (Blueprint $table) {
            $table->id("available_role_id");
            $table->bigInteger("user_id");
            $table->bigInteger("role_id");
            $table->bigInteger("create_user_id");
            $table->bigInteger("update_user_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::available_role('available_role');
    }
}
