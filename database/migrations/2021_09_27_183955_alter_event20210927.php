<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEvent20210927 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event', function (Blueprint $table) {
            $table->string('template_certificate_path', 1024)->default('');
            $table->string('template_certificate_name', 255)->default('');
            $table->string('template_certificate_mime', 50)->default('');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
