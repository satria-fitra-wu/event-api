<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterComboConstant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('combo_constant', function (Blueprint $table) {
            $table->renameColumn('id', 'combo_constant_id');
        });
        Schema::table('combo_constant_item', function (Blueprint $table) {
            $table->renameColumn('id', 'combo_constant_item_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
