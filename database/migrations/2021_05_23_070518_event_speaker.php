<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EventSpeaker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('event_speaker', function (Blueprint $table) {
            $table->id("event_speaker_id");
            $table->bigInteger("event_id")->default(-99);
            $table->bigInteger("line_no")->default(-99);
            $table->string('speaker_name', 255);
            $table->text('speaker_description');
            $table->text('banner_path');
            $table->string('banner_mime', 50);
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('event_location');
    }
}
