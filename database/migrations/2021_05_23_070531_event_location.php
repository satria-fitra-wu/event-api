<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EventLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('event_location', function (Blueprint $table) {
            $table->id("event_location_id");
            $table->bigInteger("event_id")->default(-99);
            $table->bigInteger("line_no")->default(0);
            $table->bigInteger("location_type")->default(0);
            $table->text('location_url');
            $table->text('location_description');
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('event_location');
    }
}
