<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Events extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('event', function (Blueprint $table) {
            $table->id("event_id");
            $table->bigInteger("tenant_id")->default(-99);
            $table->string('event_code', 255);
            $table->string('event_name', 255);
            $table->text('event_category', 255);
            $table->date('date_from');
            $table->date('date_to');
            $table->time('time_from');
            $table->time('time_to');
            $table->text('event_description');
            $table->text('banner_path');
            $table->string('banner_mime', 50);
            $table->bigInteger('organizer_user_id')->default(-99);
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('event');
    }
}
