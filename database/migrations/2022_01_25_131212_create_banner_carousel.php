<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerCarousel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_carousel', function (Blueprint $table) {
            $table->id('banner_carousel_id');
            $table->string('event_code', 255)->nullable();
            $table->string('banner_carousel_name', 255);
            $table->text('file_path');
            $table->string('file_name', 250);
            $table->string('file_mime', 50);
            $table->bigInteger('create_user_id')->default(-99);
            $table->bigInteger('update_user_id')->default(-99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_carousel');
    }
}
