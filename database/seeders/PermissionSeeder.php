<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Permission::create(["name" => "ADD_ROLE", "guard_name" => "admin"]);
        Permission::create(["name" => "DELETE_ROLE", "guard_name" => "admin"]);
        Permission::create(["name" => "ADD_PERMISSION", "guard_name" => "admin"]);
        Permission::create(["name" => "DELETE_PERMISSION", "guard_name" => "admin"]);
        Permission::create(["name" => "MANAGE_ROLE", "guard_name" => "admin"]);
        Permission::create(["name" => "VIEW_ROLE", "guard_name" => "admin"]);
        Permission::create(["name" => "VIEW_PERMISSION", "guard_name" => "admin"]);
        Permission::create(["name" => "ADD_USER", "guard_name" => "admin"]);
        Permission::create(["name" => "MANAGE_USER", "guard_name" => "admin"]);
    }
}
