<?php

namespace Database\Seeders;

use App\Models\ComboConstantItem;
use Illuminate\Database\Seeder;

class EventCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "KEDOKTERAN",
            "combo_constant_item_name" => "Kedokteran", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "FARMASI",
            "combo_constant_item_name" => "Farmasi", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "KEBIDANAN",
            "combo_constant_item_name" => "Kebidanan", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "KEUANGAN",
            "combo_constant_item_name" => "Keuangan", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PAJAK",
            "combo_constant_item_name" => "Pajak", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PERIKANAN",
            "combo_constant_item_name" => "Perikanan", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "KELAUTAN",
            "combo_constant_item_name" => "Kelautan", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PETERNAKAN",
            "combo_constant_item_name" => "Peternakan", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PERKEBUNAN",
            "combo_constant_item_name" => "Perkebunan", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PERTAMBANGAN",
            "combo_constant_item_name" => "Pertambangan", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "KEHUTANAN",
            "combo_constant_item_name" => "Kehutanan", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PSIKOLOGI",
            "combo_constant_item_name" => "Psikologi", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PESANTREN",
            "combo_constant_item_name" => "Pesantren", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "ANAK",
            "combo_constant_item_name" => "Anak", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "REMAJA",
            "combo_constant_item_name" => "Remaja", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "MAHASISWA",
            "combo_constant_item_name" => "Mahasiswa", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PENDIDIKAN",
            "combo_constant_item_name" => "Pendidikan", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PENSIUN",
            "combo_constant_item_name" => "Pensiun", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PERKOTAAN",
            "combo_constant_item_name" => "Perkotaan", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "MANAJERIAL",
            "combo_constant_item_name" => "Manajerial", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "TEKNOLOGI",
            "combo_constant_item_name" => "Teknologi", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PROGRAMMING",
            "combo_constant_item_name" => "Programming", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "DESAIN",
            "combo_constant_item_name" => "Desain", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "SENI",
            "combo_constant_item_name" => "Seni", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "ARSITEKTUR",
            "combo_constant_item_name" => "Arsitektur", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PEMASARAN",
            "combo_constant_item_name" => "Pemasaran", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PEMASARAN_INTERNET",
            "combo_constant_item_name" => "Pemasaran Internet", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "STARTUP",
            "combo_constant_item_name" => "Startup", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "PROPERTI",
            "combo_constant_item_name" => "Properti", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "GAYA_HIDUP",
            "combo_constant_item_name" => "Gaya Hidup", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "HIBURAN",
            "combo_constant_item_name" => "Hiburan", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "MOTIVASI",
            "combo_constant_item_name" => "Motivasi", "flg_system" => "Y", "active" => "Y"]);

        ComboConstantItem::create(["combo_constant_id" => 1, "combo_constant_item_code" => "LEADERSHIP",
            "combo_constant_item_name" => "Leadership", "flg_system" => "Y", "active" => "Y"]);

    }
}
