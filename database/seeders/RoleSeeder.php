<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::create(["name" => "SUPERADMIN", "guard_name" => "admin"]);
        Role::create(["name" => "ADMIN", "guard_name" => "admin"]);
        Role::create(["name" => "MUSRIFAH", "guard_name" => "admin"]);
        Role::create(["name" => "INTERVIEWER", "guard_name" => "admin"]);
        Role::create(["name" => "MAHASISWA", "guard_name" => "user"]);
    }
}
