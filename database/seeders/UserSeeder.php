<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //admin
        $superadmin = User::create([
            //"tenant_id" => -99,
            'tenant_id' => 10,
            'username' => 'admin',
            'full_name' => 'admin',
            'email' => 'email@email.com',
            'phone' => '0876',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password,
            'gender' => 'F',
            'address' => 'Semarang',
            'active' => 'Y',
            "create_user_id" => -1,
            "update_user_id" => -1,
        ]);

        //$superadmin->syncRoles("SUPERADMIN");
        /*
    $admin = User::create([
    "tenant_id" => -99,
    'username' => "admin",
    'full_name' => "admin",
    'email' => "admin@mail.com",
    'email_verified_at' => now(),
    'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
    'remember_token' => "",
    "create_user_id" => -99,
    "update_user_id" => -99,
    ]);
    $admin -> syncRoles("ADMIN");

    $siswa = User::create([
    //"tenant_id" => -99,
    'username' => "siswa",
    'full_name' => "siswa",
    'email' => "siswa@mail.com",
    'email_verified_at' => now(),
    'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
    'remember_token' => "",
    "create_user_id" => -99,
    "update_user_id" => -99,
    ]);
    $siswa -> syncRoles("SISWA");
     */
    }
}
