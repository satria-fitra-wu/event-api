<?php

namespace Database\Seeders;

use App\Models\ComboConstant;
use App\Models\ComboConstantItem;
use Illuminate\Database\Seeder;

class ComboConstantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //header
        $comboConstant = ComboConstant::create(["combo_constant_code" => "CATEGORY_EVENT",
            "combo_constant_name" => "Category Event", "flg_system" => "Y", "active" => "Y"]);

        //item
        ComboConstantItem::create(["combo_constant_id" => $comboConstant->combo_constant_id, "combo_constant_item_code" => "DIKLAT_PIM",
            "combo_constant_item_name" => "DIKLAT PIM", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => $comboConstant->combo_constant_id, "combo_constant_item_code" => "MEDIS",
            "combo_constant_item_name" => "MEDIS", "flg_system" => "Y", "active" => "Y"]);
        ComboConstantItem::create(["combo_constant_id" => $comboConstant->combo_constant_id, "combo_constant_item_code" => "KEPERAWATAN",
            "combo_constant_item_name" => "MANAJERIAL", "flg_system" => "Y", "active" => "Y"]);

    }
}
