<?php

namespace App\Repository;

use App\Exceptions\CoreException;
use App\Models\Event;
use App\Models\EventLocation;
use App\Models\EventSession;
use App\Models\EventSessionTicket;
use App\Models\EventSpeaker;
use App\Models\EventTicket;
use App\Models\EventTicketOrder;
use App\Models\EventTicketOrderHistory;
use App\Repository\Repository;
use Illuminate\Support\Facades\DB;

class EventRepository implements Repository
{

    public static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            return self::$instance = new self();
        }

        return self::$instance;
    }
    /**
     * @param  Array $inputData
     * @param  Array $speakerList
     * @param  Array $ticketList
     * @param  Array $locationList
     * @return Event
     **/
    public function addEvent(array $inputData, array $speakerList, array $ticketList, array $locationList)
    {
        DB::beginTransaction();

        try {
            $event = Event::create($inputData);
            foreach ($speakerList as $speaker) {
                $speaker["event_id"] = $event->event_id;
                EventSpeaker::create($speaker);
            }
            foreach ($ticketList as $ticket) {
                $ticket["event_id"] = $event->event_id;
                EventTicket::create($ticket);
            }

            foreach ($locationList as $location) {
                $location["event_id"] = $event->event_id;
                EventLocation::create($location);
            }
            DB::commit();
            return $event;
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            throw new CoreException($e->getMessage());
            // something went wrong
        }
    }

    public function addSpeaker($inputData)
    {
        $result = EventSpeaker::create($inputData);
        return $result;
    }

    public function addLocation($inputData)
    {
        $result = EventLocation::create($inputData);
        return $result;
    }

    public function addTicket($inputData)
    {
        $result = EventTicket::create($inputData);
        return $result;
    }

    public function addSession($inputData)
    {
        try {
            DB::beginTransaction();
            $result = EventSession::create($inputData);
            foreach ($inputData["sessionTicketList"] as $value) {
                $value["event_session_id"] = $result->event_session_id;
                EventSessionTicket::create($value);
            }
            DB::commit();
            return $result;

        } catch (\Exception $e) {
            DB::rollback();
            throw new CoreException($e->getMessage());

        }

    }

    public function editSession($inputData, $sessionTicketList)
    {
        DB::beginTransaction();
        try {
            $inputData->save();
            EventSessionTicket::where("event_session_id", $inputData->event_session_id)->delete();
            foreach ($sessionTicketList as $sessionTicket) {
                $sessionTicket["event_session_id"] = $inputData->event_session_id;
                EventSessionTicket::create($sessionTicket);
            }
            DB::commit();
            return $inputData;
        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();
            throw new CoreException($e->getMessage());
        }

    }

    public function editEventTicketOrder($inputData)
    {
        DB::beginTransaction();
        try {

            $inputData->save();
            $ticketOrder = $this->findEventTicketOrderById($inputData->event_ticket_order_id);
            EventTicketOrderHistory::create($ticketOrder->toArray());
            DB::commit();
            return $ticketOrder;
        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();
            throw new CoreException($e->getMessage());
        }

    }

    // public function findEventById($id)
    // {
    //     $user = Event::find($id);
    //     if (is_null($user)) {
    //         throw new CoreException(DATA_NOT_FOUND, ["0" => "Event"]);
    //     }
    //     return $user;
    // }

    // public function getTicketListByEventId($eventId)
    // {
    //     $result = EventTicket::selectRaw("event_id, event_ticket_id, ticket_name,
    //     ticket_price, ticket_description, date_from,date_to, quota")
    //         ->where("event_id", $eventId)->orderBy("created_at", "ASC")->get();
    //     return $result;
    // }

    // public function getLocationListByEventId($eventId)
    // {
    //     $result = EventLocation::selectRaw("event_id, event_location_id,location_name,location_type,
    //     location_url, location_description")
    //         ->where("event_id", $eventId)->orderBy("created_at", "ASC")->get();
    //     return $result;
    // }

    // public function getSpeakerListByEventId($eventId)
    // {
    //     $result = EventSpeaker::selectRaw("event_id, event_speaker_id,speaker_name,speaker_description")
    //         ->where("event_id", $eventId)->orderBy("created_at", "ASC")->get();
    //     return $result;
    // }

    // public function getLastLineNoSpeaker($eventId)
    // {
    //     $lineNo = EventSpeaker::selectRaw("line_no")
    //         ->where("event_id", $eventId)->orderBy("line_no", "desc")->first();
    //     $result = $lineNo->line_no;
    //     if (!is_null($result)) {
    //         $result = 0;
    //     }
    //     return $result;
    // }

    // public function getLastLineNoTicket($eventId)
    // {
    //     $lineNo = EventTicket::selectRaw("line_no")
    //         ->where("event_id", $eventId)->orderBy("line_no", "desc")->first();

    //     if (is_null($lineNo)) {
    //         $result = 0;
    //     } else {
    //         $result = $lineNo->line_no;
    //     }
    //     return $result;
    // }

    // public function getLastLineNoLocation($eventId)
    // {
    //     $lineNo = EventLocation::selectRaw("line_no")
    //         ->where("event_id", $eventId)->orderBy("line_no", "desc")->first();
    //     $result = $lineNo->line_no;
    //     if (!is_null($result)) {
    //         $result = 0;
    //     }
    //     return $result;
    // }

    public function getLastLineNoSession($eventId)
    {
        $lineNo = EventSession::selectRaw("line_no")
            ->where("event_id", $eventId)->orderBy("line_no", "desc")->first();
        if (is_null($lineNo)) {
            $result = 0;
        } else {
            $result = $lineNo->line_no;
        };

        return $result;
    }

    public function getSessionListByEventId($eventId)
    {
        $resultList = EventSession::selectRaw("event_id, event_session_id, session_name,
        session_description, line_no, file_path")
            ->where("event_id", $eventId)->orderBy("line_no", "ASC")->get();
        foreach ($resultList as $result) {
            $sessionTicketList = EventSessionTicket::where("event_session_id", $result->event_session_id)->get();
            $ticketList = [];
            foreach ($sessionTicketList as $sessionTicket) {
                $ticketList[] = $sessionTicket->event_ticket_id;
            }
            $result->ticketList = $ticketList;
        }
        return $resultList;
    }
    // public function findEventTicketById($id)
    // {
    //     $result = EventTicket::find($id);
    //     if (is_null($result)) {
    //         throw new CoreException(DATA_NOT_FOUND, ["0" => "Ticket"]);
    //     }
    //     return $result;
    // }

    public function findEventTicketOrderById($id)
    {
        $result = EventTicketOrder::find($id);
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Ticket Order"]);
        }
        return $result;
    }

    public function findEventSpeakerById($id)
    {
        $result = EventSpeaker::find($id);
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Speaker"]);
        }
        return $result;
    }

    // public function findEventLocationById($id)
    // {
    //     $result = EventLocation::find($id);
    //     if (is_null($result)) {
    //         throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Location"]);
    //     }

    //     return $result;
    // }

    public function findEventSessionById($id)
    {
        $result = EventSession::find($id);
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Session"]);
        }
        return $result;
    }

    public function getEventSessionTicketBySessionId($id)
    {
        $result = EventSessionTicket::where("event_session_id", $id)->get();
        return $result;
    }

    public function findEventTicketByIdForOrder($id)
    {
        $result = EventTicket::selectRaw("A.event_id,A.event_ticket_id,A.ticket_name, A.ticket_price, A.ticket_description, A.date_from, A.date_to, A.quota, SUM(CASE WHEN B.event_ticket_order_id IS NULL THEN 0 ELSE 1 END) AS total_order")
            ->from("event_ticket AS A")
            ->leftJoin("event_ticket_order AS B", "B.event_ticket_id", "=", "A.event_ticket_id")
            ->where("A.event_ticket_id", $id)
            ->groupByRaw("A.event_ticket_id,A.ticket_name, A.ticket_price, A.ticket_description, A.date_from, A.date_to, A.quota")
            ->first();
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Ticket"]);
        }
        return $result;
    }

    public function findEventByCode($code)
    {
        $result = Event::where("event_code", $code)->first();
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event"]);
        }
        return $result;
    }

    public function valEventExistsByCode($eventCode, $tenantId)
    {
        $result = Event::where('event_code', $eventCode)->where('tenant_id', $tenantId)->first();
        if ($result != null) {
            throw new CoreException(EVENT_WITH_CODE_ALREAY_EXISTS, ["0" => $eventCode]);
        }
    }
    public function getMyEvent($userId, $keyword, $limit, $offset, $datetime)
    {

        $q = Event::selectRaw("A.event_id, A.event_code, A.event_name, COALESCE(B.combo_constant_item_name,'') AS event_category, A.date_from, A.date_to, A.time_from, A.time_to, A.active")
            ->from("event AS A")
            ->leftJoin("combo_constant_item AS B", "B.combo_constant_item_id", "=", "A.event_category")
            ->leftJoin("combo_constant AS C", "C.combo_constant_id", "=", "B.combo_constant_id")
            ->where("A.create_user_id", $userId)
            ->where("C.combo_constant_code", "CATEGORY_EVENT");
        if ($keyword != "") {
            $q->where(function ($query) use ($keyword) {
                $query->where("A.event_code", "like", '%' . $keyword . '%')
                    ->orWhere("A.event_name", "like", '%' . $keyword . '%');
            });
        }
        $q->limit($limit)->offset($offset)->orderByRaw("A.event_code, A.event_name");
        $result = $q->get();
        return $result;
    }

    public function countGetMyEvent($userId, $keyword, $datetime)
    {
        $q = Event::selectRaw("A.event_id, A.event_code, A.event_name, A.event_category, A.date_from, A.date_to, A.time_from, A.time_to, A.active")
            ->from("event AS A")
            ->where("create_user_id", $userId);
        if ($keyword != "") {
            $q->where(function ($query) use ($keyword) {
                $query->where("A.event_code", "like", '%' . $keyword . '%')
                    ->orWhere("A.event_name", "like", '%' . $keyword . '%');
            });
        }
        $result = $q->count();
        return $result;
    }

    public function getTicketSales($eventCode, $keyword, $limit, $offset, $datetime)
    {

        $q = Event::selectRaw("A.event_id,B.event_ticket_id, C.event_ticket_order_id, A.event_code, A.event_name, B.ticket_name, C.order_code, C.name, C.email, C.instance, C.phone, C.address,
        C.order_status, C.order_status_code, C.document_path, C.document_mime")
            ->from("event AS A")
            ->join("event_ticket AS B", "B.event_id", "A.event_id")
            ->join("event_ticket_order AS C", "C.event_ticket_id", "B.event_ticket_id")
            ->where("A.event_code", $eventCode);
        if ($keyword != "") {
            $q->where(function ($query) use ($keyword) {
                $query->where("A.event_code", "like", '%' . $keyword . '%')
                    ->orWhere("A.event_name", "like", '%' . $keyword . '%')
                    ->orWhere("B.ticket_name", "like", '%' . $keyword . '%')
                    ->orWhere("C.order_code", "like", '%' . $keyword . '%');
            });
        }
        $q->limit($limit)->offset($offset)->orderByRaw("A.event_code, A.event_name, B.ticket_name, C.order_code");
        $result = $q->get();
        return $result;
    }

    public function countGetTicketSales($eventCode, $keyword, $datetime)
    {
        $q = Event::selectRaw("A.event_id,B.event_ticket_id, C.event_ticket_order_id, A.event_code, A.event_name, B.ticket_name, C.order_no, C.name, C.email, C.instance, C.phone, C.address,
        C.order_status, C.order_status_code, C.document_path, C.document_mime")
            ->from("event AS A")
            ->join("event_ticket AS B", "B.event_id", "A.event_id")
            ->join("event_ticket_order AS C", "C.event_ticket_id", "B.event_ticket_id")
            ->where("A.event_code", $eventCode);
        if ($keyword != "") {
            $q->where(function ($query) use ($keyword) {
                $query->where("A.event_code", "like", '%' . $keyword . '%')
                    ->orWhere("A.event_name", "like", '%' . $keyword . '%')
                    ->orWhere("B.ticket_name", "like", '%' . $keyword . '%')
                    ->orWhere("C.order_code", "like", '%' . $keyword . '%');
            });
        }
        $result = $q->count();
        return $result;
    }

    public function getMyTicket($userId, $keyword, $limit, $offset, $datetime)
    {
        $q = EventTicketOrder::selectRaw("A.event_ticket_order_id, A.order_code, A.order_status, A.order_status_code, B.event_name, B.date_from, B.date_to, B.time_from, B.time_to, A.remark, B.course_url")
            ->from("event_ticket_order AS A")
            ->join("event AS B", "B.event_id", "=", "A.event_id")->where("A.create_user_id", $userId);
        if ($keyword != "") {
            $q->where(function ($query) use ($keyword) {
                $query->where("A.order_code", "like", '%' . $keyword . '%')
                    ->orWhere("B.event_name", "like", '%' . $keyword . '%');
            });
        }
        $q->limit($limit)->offset($offset)->orderByRaw("A.order_code, B.event_name");
        $result = $q->get();
        return $result;
    }

    public function countGetMyTicket($userId, $keyword, $datetime)
    {
        $q = EventTicketOrder::selectRaw("A.event_ticket_order_id, A.order_code, A.order_status, A.order_status_code, B.event_name, B.date_from, B.date_to, B.time_from, B.time_to")
            ->from("event_ticket_order AS A")
            ->join("event AS B", "B.event_id", "=", "A.event_id")->where("A.create_user_id", $userId);
        if ($keyword != "") {
            $q->where(function ($query) use ($keyword) {
                $query->where("A.order_code", "like", '%' . $keyword . '%')
                    ->orWhere("B.event_name", "like", '%' . $keyword . '%');
            });
        }
        $result = $q->count();
        return $result;
    }

    public function getEventByAdvance($keyword, $limit, $offset, $datetime)
    {
        DB::enableQueryLog();
        $q = Event::selectRaw("A.event_code,A.event_name, A.event_description, A.date_from, A.date_to, A.time_from, A.time_to")->from("event AS A")
            ->where(function ($query) use ($datetime) {
                $query->whereRaw("A.date_from <= date(?)", [date('Y-m-d', strtotime($datetime))])
                    ->whereRaw("A.date_to >= date(?)", [date('Y-m-d', strtotime($datetime))]);
            });

        if (!empty($keyword)) {
            $q->where("A.event_name", "like", '%' . strtoupper($keyword) . '%');
        }
        //var_dump($q->get());
        // Enable query log
        //DB::enableQueryLog(); // Enable query log

        // Your Eloquent query executed by using get()

        //dd(DB::getQueryLog());
        // Your Eloquent query executed by using get()
        $result = $q->limit($limit)->offset($offset)->get();
        //echo "ini ya" .var_dump(DB::getQueryLog());
        return $result;
    }

    public function getTicketListForBuy($eventId, $dateTimeNow)
    {
        $result = EventTicket::selectRaw("A.event_ticket_id,A.ticket_name, A.ticket_price, A.ticket_description, A.date_from, A.date_to, A.quota, SUM(CASE WHEN B.event_ticket_order_id IS NULL THEN 0 ELSE 1 END) AS total_order")
            ->from("event_ticket AS A")
            ->leftJoin("event_ticket_order AS B", "B.event_ticket_id", "=", "A.event_ticket_id")
            ->where("A.event_id", $eventId)
            ->groupByRaw("A.event_ticket_id,A.ticket_name, A.ticket_price, A.ticket_description, A.date_from, A.date_to, A.quota")
            ->orderByRaw("A.ticket_name")
            ->get();

        foreach ($result as $data) {
            $data->flg_can_buy = true;
            $data->status = "AVAILABLE";
            if (!(strtotime($dateTimeNow) >= strtotime($data->date_from) &&
                strtotime($dateTimeNow) <= strtotime($data->date_to))) {
                $data->flg_can_buy = false;
                $data->status = "SALE ENDS";
            }

            if ($data->quota != -99 && ($data->quota < $data->total_order)) {
                $data->flg_can_buy = false;
                $data->status = "SOLD OUT";

            }
            $data->remain_ticket = $data->quota - $data->total_order;
            if ($data->quota == -99) {
                $data->remain_ticket = "UNLIMITED";
            }
        }
        return $result;
    }

    public function getTicketListForCombo($eventId)
    {
        $result = EventTicket::selectRaw("A.event_ticket_id, A.ticket_name")
            ->from("event_ticket AS A")
            ->where("A.event_id", $eventId)
            ->orderByRaw("A.ticket_name")
            ->get();
        return $result;
    }

    public function countGetEventByAdvance($keyword, $datetime)
    {
        $q = Event::from("event AS A")
            ->where(function ($query) use ($datetime) {
                $query->whereRaw("A.date_from <= date(?)", [date('Y-m-d', strtotime($datetime))])
                    ->whereRaw("A.date_to >= date(?)", [date('Y-m-d', strtotime($datetime))]);
            });

        if (!empty($keyword)) {
            $q->where("A.event_name", "like", '%' . strtoupper($keyword) . '%');
        }
        $result = $q->count();
        return $result;
    }
}
