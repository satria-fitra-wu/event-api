<?php
namespace App\Repository;

class RepositorySingleton
{
    public static $instance = null;
    public static function getInstance()
    {
        if (static::$instance == null) {
            return static::$instance = new static();
        }
        return static::$instance;
    }
    private function __construct()
    {

    }

}
