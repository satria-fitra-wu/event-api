<?php

namespace App\Repository;

use App\Exceptions\CoreException;
use App\Models\TahunAjaran;
use App\Repository\Repository;

class TahunAjaranRepository implements Repository
{

    public static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            return self::$instance = new self();
        }

        return self::$instance;
    }
    public function addTahunAjaran(array $inputData)
    {
        $user = TahunAjaran::create($inputData);
        return $user;
    }

    public function editTahunAjaran(TahunAjaran $data)
    {
        $data->update();
        return $data;
    }

    public function removeTahunAjaran(TahunAjaran $data)
    {
        $data->delete();
        return true;
    }

    public function findById($id)
    {
        $user = TahunAjaran::find($id);
        if (is_null($user)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "tahun ajaran"]);
        }
        return $user;
    }

    public function getTahunAjaranByfilter($date, $filter, $orderBy, $orderByType, $limit, $offset)
    {
        $q = TahunAjaran::whereRaw(" ? >= date_start", [$date]);
		$q ->whereRaw(" ? <= date_end", [$date]);

        if ($filter != '') {
            $q->whereRaw("(tahun_ajaran_code like '%?%' OR tahun_ajaran_name like  '%?%')", [$filter, $filter]);
        }

        $result = $q->orderBy($orderBy, $orderByType)
            ->offset($offset)
            ->limit($limit)->get();
        return $result;
    }
    public function
    countGetTahunAjaranByfilter($date, $filter)
    {
        $q = TahunAjaran::select("*");
		if ($filter != '') {
			
			$q->where(function($query) use($date){
                $query->whereDate("date_start",">=",date('Y-m-d',strtotime($date)))
				  ->whereDate("date_end","<=",date('Y-m-d',strtotime($date)));
				
            });
			$q->where("tahun_ajaran_name", "LIKE", '%' . $filter . '%');
			/*
			$q->where(function($query) use($filter){
                $query->where("tahun_ajaran_name", "LIKE", "'%".$filter."%'");
				//->OrWhere("tahun_ajaran_name", "LIKE",  "'%".$filter."%'");
				
            });*/
			
        }else{
			$q->whereDate("date_start",">=",date('Y-m-d',strtotime($date)));
			$q ->whereDate("date_end","<=",date('Y-m-d',strtotime($date)));
		}
		//$q->whereRaw("tahun_ajaran_code like '%as%' OR tahun_ajaran_name like '%as%'", []);
        
        $result = $q->count();
        return $result;
    }
}
