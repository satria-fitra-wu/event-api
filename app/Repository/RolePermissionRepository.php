<?php

namespace App\Repository;

use App\Exceptions\CoreException;
use App\Models\AvailableRole;
use App\Repository\Repository;
use Exception;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use stdClass;

class RolePermissionRepository implements Repository
{

    public static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            return self::$instance = new self();
        }

        return self::$instance;
    }
    /**
     * @param  Array $inputData["nama" => String]
     * @return Role
     * add role from array
     **/
    public function addRole(array $inputData)
    {
        $role = Role::create($inputData);
        return $role;
    }

    /**
     * @param  Role $role
     * @return Role
     * edit data role from role object with atrribute already edit
     **/
    public function editRole(Role $role)
    {
        return $role->update();
    }

    /**
     * @param  Role $role
     * @return Role
     * delete data role of role object
     **/
    public function deleteRole(Role $role)
    {
        try {
            DB::beginTransaction();
            DB::delete('delete role_has_permissions where role_id = ?', [$role->id]);
            $role->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            throw new CoreException($e);
        }
    }

    /**
     * @param  Array $inputData["nama" => String]
     * @return Permission
     * add permission from array
     **/
    public function addPermission(array $inputData)
    {
        $permission = Permission::create($inputData);
        return $permission;
    }

    /**
     * @param  Permission $permission
     * @return Permission
     * edit data permission from permission object with atrribute already edit
     **/
    public function editPermission(Permission $permission)
    {
        return $permission->update();
    }

    /**
     * @param  Permission $permission
     * @return Permission
     * delete data permission of permission object
     **/
    public function deletePermission(Permission $permission)
    {
        try {
            DB::beginTransaction();
            DB::delete('delete role_has_permissions where permission_id = ?', [$permission->id]);
            $permission->delete();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            throw new CoreException($e);
        }
    }

    /**
     * @param $id role id
     * @return Role 
     * @throws CoreException with message data not found
     * find role by id
     **/
    public function findRoleById($id)
    {
        $result = Role::where("id", $id)->first();
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "role"]);
        }
        return $result;
    }

    /**
     * @param $id role id
     * @return stdClass {isExists, role} 
     * check role exists by id
     **/
    public function isRoleExistByRoleName($roleName)
    {
        $result = new stdClass();
        $result->exists = false;
        $role = Role::where("name", $roleName)->first();
        if (!is_null($result)) {
            $result->role = $role;
        }
        return $result;
    }

    /**
     * @param $id role id
     * @return Permission 
     * @throws CoreException with message data not found
     * find permission by id
     **/
    public function findPermissionById($id)
    {
        $result = Permission::where("id", $id)->first();
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "permission"]);
        }
        return $result;
    }

    /**
     * @param $permissionName
     * @return stdClass {isExists, permission} 
     * check permissionName exists by name
     **/
    public function isPermissionExistByRoleName($permissionName)
    {
        $result = new stdClass();
        $result->exists = false;
        $permission = Permission::where("name", $permissionName)->first();
        if (!is_null($result)) {
            $result->permission = $permission;
        }
        return $result;
    }

    /**
     * @param $roleId role id
     * @return Array  list of permission
     * get permission of role id
     **/
    public function getPermissionOfRole($roleId)
    {
        return Permission::selectRaw("A.*")
            ->from("permissions AS ")
            ->join("role_has_permissions AS B", "B.permission_id", "=", "A.id")
            ->where("B.role_id", "=", $roleId)
            ->get();
    }

    public function getPermissionOfUser($userId)
    {
        return Permission::selectRaw("A.*")
            ->from("permissions AS A")
            ->join("role_has_permissions AS B", "B.permission_id", "=", "A.id")
            ->join("model_has_roles AS C", "C.role_id", "=", "B.role_id")
            ->where("C.model_id", "=", $userId)
            ->get();
    }

    public function getRole($roleName)
    {
        $q = Role::select("id, name");
        if (empty($roleName)) {
            return $q->get()->orderByRaw("name ASC");
        } else {
            return $q->where("name", $roleName)->get()->orderByRaw("name ASC");
        }
    }

    public function getPermission($permissionName)
    {
        $q = Permission::select("id, name");
        if (empty($permissionName)) {
            return $q->get()->orderByRaw("name ASC");
        } else {
            return $q->where("name", $permissionName)->get()->orderByRaw("name ASC");
        }
    }

    public function addAvailableRole(array $inputData)
    {
        $result = AvailableRole::create($inputData);
        return $result;
    }

    public function deleteAvailableRole(AvailableRole $availableRole)
    {
        return $availableRole->delete();
    }

    public function getAvailableRoleByUserId($userId)
    {
        return AvailableRole::where("user_id", $userId)->get();
    }

    public function deleteAvailableRoleByUserIdAndRoleId($userId, $roleId)
    {
        DB::delete('delete available_role where user_id = ? AND role_id = ?', [$userId, $roleId]);
        return true;
    }
}
