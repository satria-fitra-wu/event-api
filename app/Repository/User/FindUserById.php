<?php

namespace App\Repository\User;

use App\Models\User;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindUserById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = User::find($id);
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "User"]);
        }
        return $result;

    }
}
