<?php

namespace App\Repository\BannerCarousel;

use App\Models\BannerCarousel;
use Uinws\CoreApi\Repository\RepositorySingleton;

class CountGetBannerCarouselList extends RepositorySingleton
{
    public static $instance = null;
    public function execute($keyword)
    {
        $q = BannerCarousel::selectRaw("A.banner_carousel_id,A.event_code,A.banner_carousel_name")
            ->from("banner_carousel AS A");
        if (!is_null($keyword) && "" != $keyword) {
            $q->where(function ($query) use ($keyword) {
                $query->where("A.event_code", 'LIKE', '%' . $keyword . '%');
                $query->orWhere("A.banner_carousel_name", 'LIKE', '%' . $keyword . '%');
            });
        }
        $result = $q->count();
        return $result;
    }
}
