<?php

namespace App\Repository\BannerCarousel;

use App\Models\BannerCarousel;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindBannerCarouselById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = BannerCarousel::find($id);

        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Banner Carousel"]);
        }
        return $result;

    }
}
