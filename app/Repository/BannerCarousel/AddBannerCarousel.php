<?php

namespace App\Repository\BannerCarousel;

use App\Models\BannerCarousel;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddBannerCarousel extends RepositorySingleton
{
    public static $instance = null;
    public function execute($data)
    {
        return BannerCarousel::create($data);
    }
}
