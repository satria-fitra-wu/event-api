<?php

namespace App\Repository\UserOrganizer;

use App\Models\UserOrganizer;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddUserOrganizer extends RepositorySingleton
{
    public static $instance = null;
    public function execute($inputData)
    {
        $result = UserOrganizer::create($inputData);
        return $result;
    }
}
