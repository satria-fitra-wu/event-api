<?php

namespace App\Repository\UserOrganizer;

use App\Models\UserOrganizer;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindUserOrganizerById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = UserOrganizer::find($id);

        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "User Organizer"]);
        }
        return $result;
    }
}
