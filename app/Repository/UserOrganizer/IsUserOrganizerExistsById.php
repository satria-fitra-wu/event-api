<?php

namespace App\Repository\UserOrganizer;

use App\Models\UserOrganizer;
use stdClass;
use Uinws\CoreApi\Repository\RepositorySingleton;

class IsUserOrganizerExistsById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $data = UserOrganizer::where("user_id", $id)->first();
        $result = new stdClass();
        $result->exists = false;
        if ($data) {
            $result->exists = true;
            $result->data = $data;
        }
        return $result;

    }
}
