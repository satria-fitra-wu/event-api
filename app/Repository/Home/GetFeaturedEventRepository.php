<?php

namespace App\Repository\Home;

use App\Models\Event;
use App\Repository\RepositorySingleton;

class GetFeaturedEventRepository extends RepositorySingleton
{
    public static $instance = null;
    public function execute($datetime)
    {
        $q = Event::selectRaw("A.event_code,A.event_name, A.event_description, A.date_from, A.date_to, A.time_from, A.time_to")->from("event AS A")
            ->where(function ($query) use ($datetime) {
                $query->whereRaw("A.date_from <= date(?)", [date('Y-m-d', strtotime($datetime))])
                    ->whereRaw("A.date_to >= date(?)", [date('Y-m-d', strtotime($datetime))]);
            });
        $result = $q->first();
        return $result;

    }
}
