<?php

namespace App\Repository\Home;

use App\Models\BannerCarousel;
use App\Repository\RepositorySingleton;

class GetTopSellingEventRepository extends RepositorySingleton
{
    public static $instance = null;
    public function execute($datetime)
    {
        $q = BannerCarousel::selectRaw("A.banner_carousel_id,A.event_code, A.file_name")->from("banner_carousel AS A")
            ->orderBy("file_name", "ASC");
        $result = $q->get();
        return $result;

    }
}
