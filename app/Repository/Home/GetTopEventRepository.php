<?php

namespace App\Repository\Home;

use App\Models\Event;
use Illuminate\Support\Facades\DB;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetTopEventRepository extends RepositorySingleton
{
    public static $instance = null;
    public function execute($datetime, $flgFree, $flgOnline, $flgOfline)
    {
        $q = Event::selectRaw("A.event_id, A.event_code,A.event_name, A.event_description, MIN(C.event_date) AS event_date, B.location_type, E.organizer_name")
            ->from("event AS A")
            ->join("user_organizer AS E", "E.user_id", "A.organizer_user_id")
            ->join("event_location AS B", function ($join) {
                $join->on('B.event_id', '=', 'A.event_id');
                $join->on('B.line_no', '=', DB::raw("1"));
            })
            ->join("event_date AS C", function ($join) {
                $join->on('C.event_id', '=', 'A.event_id');
            })
            ->join("event_ticket AS D", function ($join) {
                $join->on('D.event_id', '=', 'A.event_id');
            })
            ->where("A.active", FLAG_YES)
            ->where(function ($query) use ($datetime) {
                //$query->whereRaw("C.event_date >= date(?)", [date('Y-m-d', strtotime($datetime))]);
                //->whereRaw("B.date >= date(?)", [date('Y-m-d', strtotime($datetime))]);
            });
        if (FLAG_YES == $flgFree) {
            $q->where("D.ticket_price", 0);
        }
        if (FLAG_YES == $flgOnline) {
            $q->where("B.location_type", 1);
        }
        if (FLAG_YES == $flgOfline) {
            $q->where("B.location_type", 0);
        }

        $result = $q->groupByRaw("A.event_id,A.event_code,A.event_name, A.event_description, B.location_type, E.organizer_name")->
            orderBy("event_date", "DESC")->
            limit(12)->get();
        return $result;

    }
}
