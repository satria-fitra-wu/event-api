<?php

namespace App\Repository;

use App\Exceptions\CoreException;
use App\Repository\Repository;
use App\Models\User;

class UserRepository implements Repository
{

    public static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            return self::$instance = new self();
        }

        return self::$instance;
    }
    public function addUser(array $inputData)
    {
        $user = User::create($inputData);
        return $user;
    }

    public function addUserFromAcademic(array $inputData)
    {
        $user = $this->getUserByuserLogin($inputData['user_login']);
        if (is_null($user)) {
            $user = User::create($inputData);
        }
        return $user;
    }

    public function findUserById($userId)
    {
        $user = User::where("user_id", "=", $userId)
            ->first();
        if (is_null($user)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "user"]);
        }
        return $user;
    }

    public function getUserByuserLogin($userLogin)
    {
        $user = User::where("user_login", "=", $userLogin)
            ->first();
        return $user;
    }

    public function isUserExistsByUsername($username)
    {
        $result = new \stdClass();
        $result->exists = false;
        $user = User::where('username', $username)->first();
        if (!is_null($user)) {
            $result->exists = true;
            $result->userData = $user;
            return $result;
        }

        return $result;
    }

    public function isUserExistsById($id)
    {
        $result = new \stdClass();
        $result->exists = false;
        $user = User::find($id);
        if (!is_null($user)) {
            $result->exists = true;
            $result->user = $user;
            return $result;
        }

        return $result;
    }

    public function isUserExistsByEmail($email)
    {
        $result = new \stdClass();
        $result->exists = false;
        $user = User::where('email', $email)->first();
        if (!is_null($user)) {
            $result->exists = true;
            $result->userData = $user;
            return $result;
        }

        return $result;
    }
}
