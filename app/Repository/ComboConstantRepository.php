<?php
namespace App\Repository;

use App\Models\ComboConstant;
use App\Models\ComboConstantItem;

class ComboConstantRepository implements Repository
{

    public static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            return self::$instance = new self();
        }

        return self::$instance;
    }

    public function update($inputData)
    {
        $inputData->save();
    }

    public function updateComboConstantItem($inputData)
    {
        $inputData->save();
    }
    public function remove($inputData)
    {
        $inputData->delete();
    }

    public function add($data)
    {
        ComboConstant::create($data);
    }

    public function addComboConstantItem($data)
    {
        ComboConstantItem::create($data);
    }

    public function getComboConstantPaginate($count, $filter = "")
    {
        $q = ComboConstant::orderByRaw("combo_constant_code ASC, combo_constant_name DESC");
        if ($filter != EMPTY_STRING) {
            $q->where(function ($query) use ($filter) {
                $query->where('A.combo_constant_code', '=', $filter)->orWhere('A.combo_constant_name', '=', $filter);
            });
        }
        return $q->paginate($count);
    }

    public function getComboConstantItemPaginateByComboContantId($count, $comboConstantId, $filter = "")
    {
        $q = ComboConstantItem::where("combo_constant_id", "=", $comboConstantId)->orderByRaw("combo_constant_item_code ASC, combo_constant_item_name DESC");
        if ($filter != EMPTY_STRING) {
            $q->where(function ($query) use ($filter) {
                $query->where('A.combo_constant_item_code', '=', $filter)->orWhere('A.combo_constant_item_name', '=', $filter);
            });
        }
        return $q->paginate($count);
    }

    public static function getComboConstantItemByComboContantCodeForHelper($comboConstantCode)
    {
        $q = ComboConstantItem::selectRaw(" combo_constant_item_code AS code, combo_constant_item_name AS name ")
            ->from("combo_constant_item as A")
            ->join("combo_constant", "combo_constant.id", "=", "A.combo_constant_id")
            ->where("combo_constant.combo_constant_code", "=", $comboConstantCode)
            ->orderByRaw("combo_constant_item_code ASC")
            ->get();
        return $q;
    }

    public static function getComboConstantItemListForCombo($comboConstantCode, $active)
    {
        $q = ComboConstantItem::selectRaw("A.combo_constant_item_id, A.combo_constant_item_code, A.combo_constant_item_name ")
            ->from("combo_constant_item as A")
            ->join("combo_constant", "combo_constant.combo_constant_id", "=", "A.combo_constant_id")
            ->where("combo_constant.combo_constant_code", "=", $comboConstantCode);
        if ($active != "ALL") {
            $q->where("A.active", "=", $active);
        }

        $result = $q->orderByRaw("combo_constant_item_code ASC, combo_constant_item_name ASC")
            ->get();
        return $result;
    }

    public static function getComboConstantItemByComboConstantCodeAndItemCode($comboConstantCode, $comboConstantItemCode)
    {
        $q = ComboConstantItem::selectRaw(" combo_constant_item_code AS code, combo_constant_item_name AS name ")
            ->from("combo_constant_item as A")
            ->join("combo_constant", "combo_constant.combo_constant_id", "=", "A.combo_constant_id")
            ->where("combo_constant.combo_constant_code", "=", $comboConstantCode)
            ->where("A.combo_constant_item_code", "=", $comboConstantItemCode)
            ->orderByRaw("combo_constant_item_code ASC")
            ->first();

        return $q;
    }

    public function isComboConstantExistsByCode($comboConstantCode)
    {
        $result = new \stdClass();
        $result->exists = false;
        $comboConstant = ComboConstant::where('combo_constant_code', $comboConstantCode)->first();
        if (!is_null($comboConstant)) {
            $result->exists = true;
            $result->comboConstant = $comboConstant;
            return $result;
        }

        return $result;
    }

    public function isComboConstantExistsById($id)
    {
        $result = new \stdClass();
        $result->exists = false;
        $comboConstant = ComboConstant::where('id', $id)->first();
        if (!is_null($comboConstant)) {
            $result->exists = true;
            $result->comboConstant = $comboConstant;
            return $result;
        }

        return $result;
    }

    public function isComboConstantItemExistsByIndex($comboConstantId, $code)
    {
        $result = new \stdClass();
        $result->exists = false;
        $comboConstantItem = ComboConstantItem::where('combo_constant_id', $comboConstantId)
            ->where("combo_constant_item", $code)->first();
        if (!is_null($comboConstantItem)) {
            $result->exists = true;
            $result->comboConstantItem = $comboConstantItem;
            return $result;
        }

        return $result;
    }

    public function isComboConstantItemExistsById($id)
    {
        $result = new \stdClass();
        $result->exists = false;
        $comboConstantItem = ComboConstantItem::find($id);
        if (!is_null($comboConstantItem)) {
            $result->exists = true;
            $result->comboConstantItem = $comboConstantItem;
            return $result;
        }

        return $result;
    }

}
