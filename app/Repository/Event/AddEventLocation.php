<?php

namespace App\Repository\Event;

use App\Models\EventLocation;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddEventLocation extends RepositorySingleton
{
    public static $instance = null;
    public function execute($inputData)
    {
        $result = EventLocation::create($inputData);
        return $result;
    }
}
