<?php

namespace App\Repository\Event;

use App\Models\EventTicketOrder;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindEventTicketOrderById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = EventTicketOrder::find($id);
        //var_dump($result);

        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Ticket Order"]);
        }
        return $result;

    }
}
