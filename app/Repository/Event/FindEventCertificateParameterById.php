<?php

namespace App\Repository\Event;

use App\Models\EventCertificateParameter;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindEventCertificateParameterById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = EventCertificateParameter::find($id);

        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Certificate Parameter"]);
        }
        return $result;

    }
}
