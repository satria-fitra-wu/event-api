<?php

namespace App\Repository\Event;

use App\Models\EventDate;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddEventDate extends RepositorySingleton
{
    public static $instance = null;
    public function execute($data)
    {
        $result = EventDate::create($data);
        return $result;
    }
}
