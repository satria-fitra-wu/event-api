<?php

namespace App\Repository\Event;

use App\Models\EventDate;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetEventDateListByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($evenId)
    {
        $q = EventDate::selectRaw("A.event_date_id, A.event_id, A.event_date, A.time_from, A.time_to")
            ->from("event_date AS A")
            ->where("A.event_id", $evenId)
            ->orderBy("A.event_date", "ASC");
        return $q->get();
    }
}
