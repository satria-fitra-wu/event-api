<?php

namespace App\Repository\Event;

use App\Models\EventSpeaker;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetEventSpeakerListByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        $result = EventSpeaker::selectRaw("event_id, event_speaker_id,speaker_name,speaker_description, banner_path")
            ->where("event_id", $eventId)->orderBy("created_at", "ASC")->get();
        return $result;

    }
}
