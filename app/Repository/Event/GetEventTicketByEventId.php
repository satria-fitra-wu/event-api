<?php

namespace App\Repository\Event;

use App\Models\EventTicket;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetEventTicketByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        return EventTicket::selectRaw("A.event_ticket_id, A.ticket_name, A.ticket_price, A.ticket_description, A.quota, COUNT(B.event_ticket_id) AS total_order")
            ->from("event_ticket AS A")
            ->leftJoin("event_ticket_order AS B", "B.event_ticket_id", "A.event_ticket_id")
            ->where("A.event_id", $eventId)
            ->whereRaw("COALESCE(B.order_status,'')<> ?", [ORDER_STATUS_REJECTED])
            ->groupByRaw("A.event_ticket_id, A.ticket_name, A.ticket_price, A.ticket_description, A.quota")
            ->orderBy("A.ticket_name", "DESC")->get();

    }
}
