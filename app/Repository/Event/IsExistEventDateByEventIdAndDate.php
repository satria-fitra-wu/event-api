<?php

namespace App\Repository\Event;

use App\Models\EventDate;
use stdClass;
use Uinws\CoreApi\Repository\RepositorySingleton;

class IsExistEventDateByEventIdAndDate extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId, $eventDate)
    {
        $data = EventDate::where("event_id", $eventId)->where("event_date", $eventDate)->first();
        $result = new stdClass();
        $result->exists = false;
        if ($data) {
            $result->exists = true;
            $result->data = $data;
        }
        return $result;

    }
}
