<?php

namespace App\Repository\Event;

use App\Models\EventLocation;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetLocationListByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        $result = EventLocation::selectRaw("event_id, event_location_id,location_name,location_type,
        location_url, location_description")
            ->where("event_id", $eventId)->orderBy("created_at", "ASC")->get();
        return $result;

    }
}
