<?php

namespace App\Repository\Event;

use App\Models\Event;
use App\Models\EventDate;
use App\Models\EventLocation;
use App\Models\EventSpeaker;
use App\Models\EventTicket;
use Exception;
use Illuminate\Support\Facades\DB;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddEvent extends RepositorySingleton
{
    public static $instance = null;
    public function execute(array $inputData, array $speakerList, array $ticketList, array $locationList, array $dateList)
    {
        DB::beginTransaction();

        try {
            $event = Event::create($inputData);
            foreach ($dateList as $date) {
                $date["event_id"] = $event->event_id;
                EventDate::create($date);
            }
            foreach ($speakerList as $speaker) {
                $speaker["event_id"] = $event->event_id;
                EventSpeaker::create($speaker);
            }
            foreach ($ticketList as $ticket) {
                $ticket["event_id"] = $event->event_id;
                EventTicket::create($ticket);
            }

            foreach ($locationList as $location) {
                $location["event_id"] = $event->event_id;
                EventLocation::create($location);
            }
            DB::commit();
            return $event;
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            throw new CoreException($e->getMessage());
        }
    }
}
