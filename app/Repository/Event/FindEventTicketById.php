<?php

namespace App\Repository\Event;

use App\Models\EventTicket;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindEventTicketById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = EventTicket::find($id);

        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Ticket"]);
        }
        return $result;

    }
}
