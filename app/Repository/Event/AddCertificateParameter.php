<?php

namespace App\Repository\Event;

use App\Models\EventCertificateParameter;
use Exception;
use Illuminate\Support\Facades\DB;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddCertificateParameter extends RepositorySingleton
{
    public static $instance = null;
    public function execute($dataJson){
        return EventCertificateParameter::create($dataJson);
    }
}
