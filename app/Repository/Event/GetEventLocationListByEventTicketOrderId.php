<?php

namespace App\Repository\Event;

use App\Models\EventLocation;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetEventLocationListByEventTicketOrderId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($event_id)
    {
        $result = EventLocation::selectRaw("event_location_id,location_name,location_type,
        location_url, location_description")
            ->where("event_id", $event_id)->orderBy("created_at", "ASC")->get();
        return $result;

    }
}
