<?php

namespace App\Repository\Event;

use App\Models\EventTicket;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddEventTicket extends RepositorySingleton
{
    public static $instance = null;
    public function execute($inputData)
    {
        $result = EventTicket::create($inputData);
        return $result;
    }
}
