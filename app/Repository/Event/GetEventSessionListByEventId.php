<?php

namespace App\Repository\Event;

use App\Models\EventSession;
use App\Models\EventSessionTicket;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetEventSessionListByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        $resultList = EventSession::selectRaw("event_id, event_session_id, session_name,
        session_description, line_no, file_path")
            ->where("event_id", $eventId)->orderBy("line_no", "ASC")->get();
        foreach ($resultList as $result) {
            $sessionTicketList = EventSessionTicket::where("event_session_id", $result->event_session_id)->get();
            $ticketList = [];
            foreach ($sessionTicketList as $sessionTicket) {
                $ticketList[] = $sessionTicket->event_ticket_id;
            }
            $result->ticketList = $ticketList;
        }
        return $resultList;

    }
}
