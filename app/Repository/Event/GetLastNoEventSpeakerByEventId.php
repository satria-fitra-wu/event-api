<?php

namespace App\Repository\Event;

use App\Models\EventSpeaker;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetLastNoEventSpeakerByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        $lineNo = EventSpeaker::selectRaw("line_no")
            ->where("event_id", $eventId)->orderBy("line_no", "desc")->first();
        $result = is_null($lineNo) ? 0 : $lineNo->line_no;
        if (!is_null($result)) {
            $result = 0;
        }
        return $result;

    }
}
