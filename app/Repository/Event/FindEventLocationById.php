<?php

namespace App\Repository\Event;

use App\Models\EventLocation;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindEventLocationById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {$result = EventLocation::find($id);
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Location"]);
        }

        return $result;
    }
}
