<?php

namespace App\Repository\Event;

use App\Models\EventSpeaker;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindEventSpeakerById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = EventSpeaker::find($id);

        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Speaker"]);
        }
        return $result;

    }
}
