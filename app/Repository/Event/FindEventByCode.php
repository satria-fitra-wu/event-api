<?php

namespace App\Repository\Event;

use App\Models\Event;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindEventByCode extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventCode)
    {
        $result = Event::where("event_code", $eventCode)->first();

        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event"]);
        }
        return $result;

    }
}
