<?php

namespace App\Repository\Event;

use App\Models\EventTicket;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetLastLineNoEventTicketByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        $lineNo = EventTicket::selectRaw("line_no")
            ->where("event_id", $eventId)->orderBy("line_no", "desc")->first();

        if (is_null($lineNo)) {
            $result = 0;
        } else {
            $result = $lineNo->line_no;
        }
        return $result;

    }
}
