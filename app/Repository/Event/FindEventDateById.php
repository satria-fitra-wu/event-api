<?php

namespace App\Repository\Event;

use App\Models\EventDate;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindEventDateById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = EventDate::find($id);
        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event Date"]);
        }
        return $result;

    }
}
