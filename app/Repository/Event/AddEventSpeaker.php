<?php

namespace App\Repository\Event;

use App\Models\EventSpeaker;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddEventSpeaker extends RepositorySingleton
{
    public static $instance = null;
    public function execute($inputData)
    {
        $result = EventSpeaker::create($inputData);
        return $result;

    }
}
