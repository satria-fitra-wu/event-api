<?php

namespace App\Repository\Event;

use App\Models\EventDate;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetEventDateByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        return EventDate::where("event_id", $eventId)->orderBy("event_date", "DESC")->get();
    }
}
