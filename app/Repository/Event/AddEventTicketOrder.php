<?php

namespace App\Repository\Event;

use App\Models\EventTicketOrder;
use App\Models\EventTicketOrderHistory;
use Exception;
use Illuminate\Support\Facades\DB;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class AddEventTicketOrder extends RepositorySingleton
{
    public static $instance = null;
    public function execute($inputData)
    {
        DB::beginTransaction();
        try {
            $eventTicketOrder = EventTicketOrder::create($inputData);
            EventTicketOrderHistory::create($eventTicketOrder->toArray());
            DB::commit();
            return $eventTicketOrder;
        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();
            throw new CoreException($e->getMessage());
        }

    }
}
