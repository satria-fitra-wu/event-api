<?php

namespace App\Repository\Event;

use App\Models\EventTicketOrder;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetLastTicketOrderLineNo extends RepositorySingleton
{
    public static $instance = null;
    public function execute($ticketId)
    {
        $ticketOrder = EventTicketOrder::selectRaw("line_no")
            ->where("event_ticket_id", $ticketId)
            ->orderByRaw("line_no DESC")->first();
        if (is_null($ticketOrder)) {
            return 0;
        }
        return $ticketOrder->line_no;
    }
}
