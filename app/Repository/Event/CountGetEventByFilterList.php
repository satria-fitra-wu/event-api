<?php

namespace App\Repository\Event;

use App\Models\Event;
use Illuminate\Support\Facades\DB;
use Uinws\CoreApi\Repository\RepositorySingleton;

class CountGetEventByFilterList extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventDate, $eventCategoryId, $eventTicketCategory, $eventLocationCategory, $eventOrganizer, $keyword)
    {
        $q = Event::selectRaw("A.event_code, A.event_name, A.event_description, MIN(C.event_date) AS event_date, B.location_type")
            ->from("event AS A")
            ->join("user_organizer AS E", "E.user_id", "A.organizer_user_id")
            ->join("event_location AS B", function ($join) {
                $join->on('B.event_id', '=', 'A.event_id');
                $join->on('B.line_no', '=', DB::raw("1"));
            })
            ->join("event_date AS C", function ($join) {
                $join->on('C.event_id', '=', 'A.event_id');
            })
            ->join("event_ticket AS D", function ($join) {
                $join->on('D.event_id', '=', 'A.event_id');
            });
        $q->where("A.active", FLAG_YES);
        if (!is_null($eventDate) && $eventDate != "") {
            $q->where(function ($query) use ($eventDate) {
                $query->whereRaw("C.event_date = date(?)", [date('Y-m-d', strtotime($eventDate))]);
            });
        }

        if (!is_null($eventOrganizer) && $eventOrganizer != "") {
            //echo "masuk pak ";
            //var_dump()
            $q->where(function ($query) use ($eventOrganizer) {
                $query->where("E.organizer_name", 'LIKE', '%' . $eventOrganizer . '%');
            });
        }

        if (NULL_REF_INT != $eventCategoryId) {
            $q->where("A.event_category", $eventCategoryId);
        }

        if ("FREE" == $eventTicketCategory) {
            $q->where("D.ticket_price", 0);
        }

        if ("PAY" == $eventTicketCategory) {
            $q->where("D.ticket_price", "<>", 0);
        }

        if (NULL_REF_INT != $eventLocationCategory) {
            $q->where("B.location_type", $eventLocationCategory);
        }

        if (!is_null($keyword) && "" != $keyword) {
            $q->where(function ($query) use ($keyword) {
                $query->where("A.event_name", 'LIKE', '%' . $keyword . '%');
            });
        }
        $q->groupByRaw("A.event_code, A.event_name, A.event_description, B.location_type");

        //$result = $q->count();
        $result = $q->get();
        //var_dump($result);

        return count($result);

    }
}
