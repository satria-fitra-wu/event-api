<?php

namespace App\Repository\Event;

use App\Models\Event;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindEventById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id)
    {
        $result = Event::find($id);

        if (is_null($result)) {
            throw new CoreException(DATA_NOT_FOUND, ["0" => "Event"]);
        }
        return $result;

    }
}
