<?php

namespace App\Repository\Event;

use App\Models\EventTicketOrder;
use stdClass;
use Uinws\CoreApi\Repository\RepositorySingleton;

class IsExistsEventTicketOrderByticketIdAndUserId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($ticketId, $userId)
    {
        $result = new stdClass();
        $result->exists = false;
        $ticketOrder = EventTicketOrder::where("event_ticket_id", $ticketId)->where("order_user_id", $userId)->first();
        if (!is_null($ticketOrder)) {
            $result->exists = true;
            $result->eventTicketOrder = $ticketOrder;
        }
        return $result;
    }
}
