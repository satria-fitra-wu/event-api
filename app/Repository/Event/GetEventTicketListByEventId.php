<?php

namespace App\Repository\Event;

use App\Models\EventTicket;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetEventTicketListByEventId extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        $result = EventTicket::selectRaw("event_id, event_ticket_id, ticket_name,
        ticket_price, ticket_description, date_from,date_to, quota")
            ->where("event_id", $eventId)->orderBy("created_at", "ASC")->get();
        return $result;

    }
}
