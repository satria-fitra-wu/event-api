<?php

namespace App\Repository\Event;

use App\Models\Event;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Repository\RepositorySingleton;

class ValEventExistsByCode extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventCode, $tenantId)
    {
        $result = Event::where('event_code', $eventCode)->where('tenant_id', $tenantId)->first();
        if ($result != null) {
            throw new CoreException(EVENT_WITH_CODE_ALREAY_EXISTS, ["0" => $eventCode]);
        }

    }
}
