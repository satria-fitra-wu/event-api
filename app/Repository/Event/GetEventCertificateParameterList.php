<?php

namespace App\Repository\Event;

use App\Models\EventCertificateParameter;
use Uinws\CoreApi\Repository\RepositorySingleton;

class GetEventCertificateParameterList extends RepositorySingleton
{
    public static $instance = null;
    public function execute($eventId)
    {
        $result = EventCertificateParameter::selectRaw("A.event_certificate_parameter_id,A.event_id,
        A.parameter_name, A.parameter_type, A.parameter_value, A.flg_dynamic_val, A.default_value, A.align,
        A.position_x, A.position_y, A.width, A.height, A.font_size")
            ->from("event_certificate_parameter AS A")

            ->where("event_id", $eventId)->orderBy("A.event_certificate_parameter_id", "ASC")->get();
        return $result;
    }
}
