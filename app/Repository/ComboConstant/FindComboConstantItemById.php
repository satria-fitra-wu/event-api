<?php

namespace App\Repository\ComboConstant;

use App\Exceptions\CoreException;
use App\Models\ComboConstantItem;
use Exception;
use Illuminate\Support\Facades\DB;
use Uinws\CoreApi\Repository\RepositorySingleton;

class FindComboConstantItemById extends RepositorySingleton
{
    public static $instance = null;
    public function execute($id){
        $result = ComboConstantItem::find($id);
        if(!$result){
            throw new CoreException(DATA_NOT_FOUND,["0"=>"Combo Constant Item"]);
        }
        return $result;
    }
}
