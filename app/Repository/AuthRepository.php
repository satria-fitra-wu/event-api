<?php

namespace App\Repository;

use App\Exceptions\CoreException;
use App\Repository\Repository;
use App\Models\User;
use App\Models\UserAkademik;

class AuthRepository implements Repository
{

    public static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            return self::$instance = new self();
        }

        return self::$instance;
    }
    /**
     * @param  string $userLogin user name
     * @param  int $userPass password sudah md5
     * @return User
     **/
    public function findUserAkademikByUserLoginAndUserPass($userLogin, $userPass)
    {
        $user = UserAkademik::where("user_login", "=", $userLogin)
            ->where("user_pass", "=", $userPass)
            ->first();
        return $user;
    }
}
