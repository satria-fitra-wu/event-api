<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class OnlyJsonDataRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if ($request->getMethod() == "GET") {
            if ($request->cookie('token')) {
                $request->headers->set('Authorization', 'Bearer ' . $request->cookie('token'));
            }
            $request->headers->set('Accept', 'application/json');

        }
        $acceptHeader = $request->header('Accept');
        if ($acceptHeader != 'application/json') {
            return response()->json(["success" => false,
                "errorKey" => 406,
                "args" => "",
                "msg" => "Only Accept JSON data"], 406);
        }
        return $next($request);

    }
}
