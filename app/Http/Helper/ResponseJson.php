<?php

namespace App\Http\Helper;

use App\Exceptions\CoreException;
use Illuminate\Support\Facades\Log;

class ResponseJson
{

    /**
     * @param $json
     * @return \Illuminate\Http\JsonResponse
     */
    static function success($json)
    {
        if (!is_null($json)) {
            /*if(is_array($json)){
                $json["success"] = true;
            } else if(is_object($json)) {
                $json->success = true;
            }*/

            return response()->json([
                "data" => $json,
                "success" => true
            ]);
        } else {

            return response()->json([
                "success" => true
            ]);
        }
    }

    /**
     * @param $ex
     * @return \Illuminate\Http\JsonResponse
     */
    public static function fail($ex, $statusCode = -99)
    {
        $result = [];

        if ($ex instanceof CoreException) {
            Log::error($ex);
            //$result["success"] = false;
            $result["errorKey"] = $ex->getErrorKey();
            $result["args"] = $ex->getArgs();
            $result["msg"] = EMPTY_STRING;
            if ($statusCode <> NULL_REF_INT)
                $result["statusCode"] = $statusCode;

            if ($ex->getErrorKey() == ERROR_DATA_VALIDATION) {

                $errorData = $ex->getMessages();
                $converterJson = json_encode($errorData);
                $converterDecode = (array)json_decode($converterJson);
                $keys = array_keys($converterDecode);

                $errors = [];
                foreach ($keys as $value) {
                    $errors[$value] = $ex->getMessages()->first($value);
                }

                $result["listMsg"] = $errors;
            } else {
                $result["msg"] = $ex->getMessage();
            }
        } else if ($ex instanceof \Exception) {
            Log::error($ex);
            //$result["success"] = false;
            $result["errorKey"] = "";
            $result["args"] = null;
            $result["msg"] = $ex->getMessage();
        }

        return response()->json([
            "data" => $result,
            "success" => false
        ]);
    }
}
