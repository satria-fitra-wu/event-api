<?php

namespace App\Http\Controllers\BannerCarousel;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Repository\BannerCarousel\FindBannerCarouselById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class DownloadBannerCarouselImageController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $findBannerCarouselById = FindBannerCarouselById::getInstance();

            $bannerCarousel = $findBannerCarouselById->execute($request->route("id"));

            if ($bannerCarousel->file_name != $request->route("file_name")) {
                throw new CoreException("file not found");
            }
            $pathToFile = storage_path("app/public/" . $bannerCarousel->file_path);
            // $headers = array(
            //     'Content-Type: ' . $bannerCarousel->file_mime,
            // );
            $file = file_get_contents($pathToFile);

            //var_dump($file);

            //$type = File::mimeType($event->banner_mime);

            $response = Response::make($file, 200);

            $response->header("Content-Type", $bannerCarousel->file_mime);
            return $response;
            //return response()->download($pathToFile, $fileUpload->original_filename, $headers);

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }
}
