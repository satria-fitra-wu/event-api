<?php

namespace App\Http\Controllers\BannerCarousel;

use App\Http\Controllers\Controller;
use App\Repository\BannerCarousel\FindBannerCarouselById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class EditBannerCarouselController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $inputJson["banner_file"] = $request->banner_file;
            $rules = [
                "banner_carousel_id" => "required",
                "banner_carousel_name" => "required",
                "event_code" => "present",
            ];
            if (!is_null($request->banner_file)) {
                $rules['banner_file'] = 'file|image|mimes:jpeg,png,jpg|max:512';
            }

            //validasi input
            $validator = Validator::make($inputJson, $rules);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $findBannerCarouselById = FindBannerCarouselById::getInstance();

            $bannerCarousel = $findBannerCarouselById->execute($inputJson["banner_carousel_id"]);

            $bannerCarousel->event_code = is_null($inputJson["event_code"]) ? '' : $inputJson["event_code"];
            $bannerCarousel->banner_carousel_name = $inputJson["banner_carousel_name"];
            //$bannerCarousel->file_path = $preparedFile["banner_carousel_name"];
            if (!is_null($request->banner_file)) {
                $preparedFile = $this->prepareSaveFile($inputJson["banner_file"], BANNER_CAROUSEL_PATH, "file");
                $splitedPath = explode("/", $preparedFile["file_path"]);

                $bannerCarousel->file_name = $splitedPath[count($splitedPath) - 1];
                $bannerCarousel->file_mime = $preparedFile["file_mime"];

                $this->editFile($inputJson["banner_file"], $preparedFile["file_path"], $bannerCarousel->file_path);

                $bannerCarousel->file_path = $preparedFile["file_path"];
            }

            $bannerCarousel = $this->setUdateUserId($bannerCarousel);

            $bannerCarousel->save();
            return ResponseJson::success($bannerCarousel);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
