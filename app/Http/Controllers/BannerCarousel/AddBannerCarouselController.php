<?php

namespace App\Http\Controllers\BannerCarousel;

use App\Http\Controllers\Controller;
use App\Repository\BannerCarousel\AddBannerCarousel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class AddBannerCarouselController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $inputJson["banner_file"] = $request->banner_file;
            //validasi input
            $validator = Validator::make($inputJson, [
                "banner_carousel_name" => "required",
                "event_code" => "present",
                "banner_file" => "required|file|mimes:jpg,jpeg,png|max:512",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $preparedFile = $this->prepareSaveFile($inputJson["banner_file"], BANNER_CAROUSEL_PATH, "file");

            $splitedPath = explode("/", $preparedFile["file_path"]);
            $dataTobeAdded = ["event_code" => is_null($inputJson["event_code"]) ? '' : $inputJson["event_code"],
                "banner_carousel_name" => $inputJson["banner_carousel_name"],
                "file_path" => $preparedFile["file_path"],
                "file_name" => $splitedPath[count($splitedPath) - 1],
                "file_mime" => $preparedFile["file_mime"],
            ];

            $dataTobeAdded = $this->setCreateUserId($dataTobeAdded);
            $this->saveFile($inputJson["banner_file"], $dataTobeAdded["file_path"]);

            $addBannerCarousel = AddBannerCarousel::getInstance();

            $result = $addBannerCarousel->execute($dataTobeAdded);

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
