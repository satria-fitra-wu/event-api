<?php

namespace App\Http\Controllers\Home;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Http\Helper\ResponseJson;
use App\Repository\Home\GetTopEventRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class GetTopEventController extends Controller
{
    //
    public function __invoke(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "flg_free" => "required",
                "flg_online" => "required",
                "flg_offline" => "required",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $getTopEventRepository = GetTopEventRepository::getInstance();

            $dateTimeNow = $this->getDatetimeNow();
            $resultList = $getTopEventRepository->execute($dateTimeNow, $inputJson["flg_free"], $inputJson["flg_online"], $inputJson["flg_offline"]);
            foreach ($resultList as $result) {
                $result->flg_free = FLAG_NO;
                if (DB::table('event_ticket')
                    ->where('event_id', $result->event_id)
                    ->where("ticket_price", 0)
                    ->exists()) {
                    $result->flg_free = FLAG_YES;

                }
                $result->event_date = date('d-m-Y', strtotime($result->event_date));
                $result->date_from = date('d-m-Y', strtotime($result->date_from));
                $result->date_to = date('d-m-Y', strtotime($result->date_to));
                $result->event_description = substr(strip_tags($result->event_description), 0, 25);
                $result->banner_url = $this->getUrl("getBannerEvent/" . $result->event_code);
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }
}
