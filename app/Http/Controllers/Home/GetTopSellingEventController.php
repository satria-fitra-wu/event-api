<?php

namespace App\Http\Controllers\Home;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Http\Helper\ResponseJson;
use App\Repository\Home\GetTopSellingEventRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class GetTopSellingEventController extends Controller
{
    //
    public function __invoke(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $getTopSellingEventRepository = GetTopSellingEventRepository::getInstance();

            $dateTimeNow = $this->getDatetimeNow();
            $resultList = $getTopSellingEventRepository->execute($dateTimeNow);
            foreach ($resultList as $result) {
                $result->banner_url = $this->getUrl("api/bannerCarousel/" . $result->banner_carousel_id . "/" . $result->file_name);
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }
}
