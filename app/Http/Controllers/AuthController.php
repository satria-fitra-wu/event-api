<?php

namespace App\Http\Controllers;

use App\Exceptions\CoreException as CoreException;
use App\Http\Helper\ResponseJson;
use App\Repository\AuthRepository;
use App\Repository\RolePermissionRepository;
use App\Repository\UserRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public $authRepository;
    public $rolePermissionRepository;
    public $userRep;

    public function __construct(
        AuthRepository $authRepository,
        RolePermissionRepository $rolePermissionRepository,
        UserRepository $userRep
    ) {
        $this->authRepository = $authRepository::getInstance();
        $this->rolePermissionRepository = $rolePermissionRepository::getInstance();
        $this->userRep = $userRep::getInstance();
    }

    public function login(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //var_dump($inputJson);
            $validator = Validator::make($inputJson, [
                'username' => 'required',
                'password' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            if (Auth::attempt($inputJson)) {
                $user = Auth::user();
                //hapus semua token yg sudah ada
                $user->tokens->each(function ($token, $key) {
                    //$token->revoke();
                    $token->delete();
                });
                $resultData['token'] = $user->createToken('admin')->accessToken;
                $resultData['display_name'] = $user->full_name;

                $permissions = $this->rolePermissionRepository->getPermissionOfUser($user->id);
                $resultData['permissions'] = $permissions;
                $resultData['verified'] = $user->hasVerifiedEmail();

                return ResponseJson::success($resultData);
            } else {
                throw new CoreException(USERNAME_AND_PASSWORD_NOT_MATCH);
            }
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }

    public function logout(Request $request)
    {
        try {
            if (Auth::check()) {
                Auth::user()->token()->delete();
                return ResponseJson::success(["msg" => "success"]);
            } else {
                throw new CoreException('');
            }
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }

    public function isTokenValid(Request $request)
    {
        try {
            if (Auth::check()) {
                return ResponseJson::success(["msg" => "success"]);
            } else {
                throw new CoreException("");
            }
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }
}
