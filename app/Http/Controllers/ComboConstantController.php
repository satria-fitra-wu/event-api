<?php

namespace App\Http\Controllers;

use App\Http\Helper\ResponseJson;
use App\Repository\ComboConstantRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

//use App\Repository\ComboConstantRepository;

class ComboConstantController extends Controller
{
    public $comboConstantRepository;

    public function __construct(ComboConstantRepository $comboConstantRepository)
    {
        $this->comboConstantRepository = $comboConstantRepository::getInstance();
    }

    public function getComboConstantItemListForCombo(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'combo_constant_code' => 'required',
                'active' => ['required', Rule::in(['Y', 'N', 'ALL'])],
            ]);

            $result = $this->comboConstantRepository->getComboConstantItemListForCombo($inputJson['combo_constant_code'], $inputJson['active']);

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

}
