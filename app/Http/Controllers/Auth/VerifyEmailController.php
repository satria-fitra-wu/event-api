<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repository\User\FindUserById;
use Exception;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Traits\UinWsBaseController;

class VerifyEmailController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $findUserById = FindUserById::getInstance();
            $user = $findUserById->execute($request->route('id'));

            if ($user->hasVerifiedEmail()) {
                throw new CoreException(EMAIL_ALREADY_VERIFIED);
            }

            if ($user->markEmailAsVerified()) {
                event(new Verified($user));
            }
            //echo "masuk pak eko";
            return Redirect::to("/successActivateEmail");

            //return ResponseJson::success(["msg"=>"THANKS, VERIFICATION EMAIL SUCCESS"]);
        } catch (Exception $e) {
            return Redirect::to("/failActivateEmail/" . $e->getMessage());

            Log::error($e);
            //return ResponseJson::fail($e);
        }
    }
}
