<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgetPasswordMail;
use App\Repository\User\IsUserExistsByEmail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class ForgetPasswordController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "email" => "required|email:rfc",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            // $status = Password::sendResetLink(
            //     $inputJson
            // );

            // Password::sendResetLink(
            //     $inputJson
            // );

            // Password::broker()->createToken();
            $isUserExistsByEmail = IsUserExistsByEmail::getInstance();

            $resultIsUserExists = $isUserExistsByEmail->execute($inputJson["email"]);

            if (!$resultIsUserExists->exists) {
                throw new CoreException(EMAIL_NOT_FOUND);
            }
            $token = DB::table('password_resets')->selectRaw("email, token, created_at")
                ->where("email", $request->email)->first();

            if (!is_null($token) && round(abs(strtotime($this->getDatetimeNow()) - strtotime($token->created_at)) / 60, 0) < 10) {
                throw new CoreException(PLEASE_WAIT_TEN_MINUTES_FOR_RESET_PASSWORD_AGAIN);
            }

            if (!is_null($token)) {
                DB::statement("DELETE FROM password_resets WHERE email = :email", ["email" => $request->email]);
            }

            $user = $resultIsUserExists->data;

            $token = Str::random(64);

            Mail::to($inputJson["email"])
                ->send(new ForgetPasswordMail([
                    "name" => $user->name,
                    "reset_url" => $this->getUrl("/resetpassword?token=" . $token . '&email=' . $request->email)]));
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => $this->getDatetimeNow(),
            ]);

            return ResponseJson::success(["msg" => "Please check your email"]);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
