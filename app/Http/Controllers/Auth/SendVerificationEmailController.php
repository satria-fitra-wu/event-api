<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class SendVerificationEmailController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            if ($request->user()->hasVerifiedEmail()) {
                throw new CoreException(EMAIL_ALREADY_VERIFIED);
            }

            $request->user()->sendEmailVerificationNotification();

            return ResponseJson::success(["msg" => "success"]);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
