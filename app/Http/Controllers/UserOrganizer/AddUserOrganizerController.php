<?php

namespace App\Http\Controllers\UserOrganizer;

use App\Http\Controllers\Controller;
use App\Models\UserOrganizer;
use App\Repository\UserOrganizer\IsUserOrganizerExistsById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class AddUserOrganizerController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "organizer_name" => "required",
                "identity_no" => "required",
                "npwp_no" => "required",
                "email" => "email:filter",
                "phone" => "required",
                "address" => "required",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $isUserOrganizerExistsById = IsUserOrganizerExistsById::getInstance();

            $isUserOrganizerExists = $isUserOrganizerExistsById->execute(Auth::id());

            if ($isUserOrganizerExists->exists) {
                $userOrganizer = $isUserOrganizerExists->data;
                $userOrganizer->organizer_name = $inputJson["organizer_name"];
                $userOrganizer->verify_status = "VERIFIED";
                $userOrganizer->identity_no = $inputJson["identity_no"];
                $userOrganizer->identity_path = "";
                $userOrganizer->identity_mime = "";
                $userOrganizer->npwp_no = $inputJson["npwp_no"];
                $userOrganizer->npwp_path = "";
                $userOrganizer->npwp_mime = "";
                $userOrganizer->email = $inputJson["email"];
                $userOrganizer->instance = "";
                $userOrganizer->phone = $inputJson["phone"];
                $userOrganizer->address = $inputJson["address"];

                $userOrganizer->save();
            } else {

                $inputData = ["organizer_name" => $inputJson["organizer_name"],
                    "verify_status" => "VERIFIED",
                    "identity_no" => $inputJson["identity_no"],
                    "identity_path" => "",
                    "identity_mime" => "",
                    "npwp_no" => $inputJson["npwp_no"],
                    "npwp_path" => "",
                    "npwp_mime" => "",
                    "email" => $inputJson["email"],
                    "instance" => "",
                    "phone" => $inputJson["phone"],
                    "address" => $inputJson["address"],
                ];
                $userOrganizer = new UserOrganizer($inputData);

                $userOrganizer->save();

            }

            return ResponseJson::success($userOrganizer);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
