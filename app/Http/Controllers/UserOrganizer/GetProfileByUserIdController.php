<?php

namespace App\Http\Controllers\UserOrganizer;

use App\Http\Controllers\Controller;
use App\Repository\UserOrganizer\IsUserOrganizerExistsById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class GetProfileByUserIdController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $isUserOrganizerExistsById = IsUserOrganizerExistsById::getInstance();
            $userOrganizerExists = $isUserOrganizerExistsById->execute(Auth::id());
            $organizer = [];
            if ($userOrganizerExists->exists) {
                $userOrganizer = $userOrganizerExists->data;
                $organizer["organizer_name"] = $userOrganizer->organizer_name;
                $organizer["identity_no"] = $userOrganizer->identity_no;
                $organizer["npwp_no"] = $userOrganizer->npwp_no;
                $organizer["email"] = $userOrganizer->email;
                $organizer["phone"] = $userOrganizer->phone;
                $organizer["address"] = $userOrganizer->address;

            }

            $user = Auth::user();
            $profile = ["full_name" => $user->full_name,
                "gender" => $user->gender,
                "phone" => $user->phone,
                "address" => $user->address];
            $result["profile"] = $profile;
            $result["organizer"] = $organizer;

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
