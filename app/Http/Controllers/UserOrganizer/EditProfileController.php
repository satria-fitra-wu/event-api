<?php

namespace App\Http\Controllers\UserOrganizer;

use App\Http\Controllers\Controller;
use App\Repository\UserOrganizer\EditUserOrganizer;
use App\Repository\UserRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class EditProfileController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'full_name' => 'required',
                'gender' => 'required|min:1',
                'address' => 'required',
                'phone'=>"required"
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $userRepository = new UserRepository();
            $user = $userRepository->findUserById(Auth::id());
            $user->full_name = $inputJson['full_name'];
            $user->gender = $inputJson['gender'];
            $user->phone = $inputJson['phone'];
            $user->address = $inputJson['address'];

            $user->save();



            return ResponseJson::success($user);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}

