<?php

namespace App\Http\Controllers;

use App\Events\CreateUserEvent;
use App\Exceptions\CoreException as CoreException;
use App\Http\Helper\ResponseJson;
use App\Repository\RolePermissionRepository;
use App\Repository\UserRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $rolePermissionRepository;
    public $userRepository;

    public function __construct(RolePermissionRepository $rolePermissionRepository, UserRepository $userRepository)
    {
        $this->rolePermissionRepository = $rolePermissionRepository::getInstance();
        $this->userRepository = $userRepository::getInstance();
    }
    //
    public function addUser(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                //'tenantId' => 'required',
                'username' => 'required|min:5',
                'full_name' => 'required',
                "email" => 'required|email:filter',
                'password' => 'required|min:5',
                'gender' => 'required|min:1',
                'address' => 'required',
                'phone' => "required",
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            if ($this->userRepository->isUserExistsByUsername($inputJson['username'])->exists) {
                throw new CoreException(USERNAME_ALREADY_EXISTS, ["0" => $inputJson['username']]);
            };

            if ($this->userRepository->isUserExistsByEmail($inputJson['email'])->exists) {
                throw new CoreException(EMAIL_ALREADY_EXISTS, ["0" => $inputJson['email']]);
            };

            $password = Hash::make($inputJson['password']);
            $dataUser = [
                "tenant_id" => -99,
                "username" => $inputJson['username'],
                "full_name" => $inputJson['full_name'],
                "email" => $inputJson['email'],
                "password" => $password,
                "gender" => $inputJson['gender'],
                "address" => $inputJson['address'],
                "phone" => $inputJson['phone'],
            ];
            $user = $this->userRepository->addUser($dataUser);
            $user->markEmailAsVerified();
            //event(new Registered($user));
            $dataEmail = ["email" => $user->email,
                "password" => $inputJson['password'],
                "username" => $user->email];
            event(new CreateUserEvent($dataEmail));

            return ResponseJson::success($user);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }

    public function addRoleToUser(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                'userId' => 'required',
                'roleId' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $role = $this->rolePermissionRepository->findRoleById($inputJson["roleId"]);
            $user = $this->userRepository->findUserById($inputJson["userId"]);

            $dataAvailableRole = [
                "user_id" => $inputJson["userId"],
                "role_id" => $inputJson["roleId"],
            ];

            $availableRole = $this->rolePermissionRepository->addAvailableRole($dataAvailableRole);

            return ResponseJson::success($availableRole);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }

    public function removeRoleFromUser(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                'userId' => 'required',
                'roleId' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $role = $this->rolePermissionRepository->deleteAvailableRoleByUserIdAndRoleId($inputJson["userId"], $inputJson["roleId"]);

            return ResponseJson::success(["msg" => SUCCESS_DELETE_DATA]);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }

    public function SetRoleToCurrentUser(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                'userId' => 'required',
                'roleId' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $role = $this->rolePermissionRepository->findRoleById($inputJson["roleId"]);
            $user = Auth::user();

            $user->syncRoles($role);

            return ResponseJson::success(["msg" => "success"]);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }
}
