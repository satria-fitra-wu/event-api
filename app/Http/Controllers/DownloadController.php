<?php

namespace App\Http\Controllers;

use App\Http\Helper\ResponseJson;
use App\Repository\EventRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class DownloadController extends Controller
{
    //
    public $eventRepository;

    // public $userRepository;
    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository::getInstance();
    }

    public function getBannerEvent(Request $request, $code)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $event = $this->eventRepository->findEventByCode($code);
            $pathToFile = storage_path("app/public/" . $event->banner_path);
            $headers = array(
                'Content-Type: ' . $event->banner_mime,
            );
            $file = file_get_contents($pathToFile);

            //var_dump($file);

            //$type = File::mimeType($event->banner_mime);

            $response = Response::make($file, 200);

            $response->header("Content-Type", $event->banner_mime);
            return $response;
            //return response()->download($pathToFile, $fileUpload->original_filename, $headers);

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function getSpeakerPhoto(Request $request, $speakerId)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $speaker = $this->eventRepository->findEventSpeakerById($speakerId);
            $pathToFile = storage_path("app/public/" . $speaker->banner_path);
            $headers = array(
                'Content-Type: ' . $speaker->banner_mime,
            );
            $file = file_get_contents($pathToFile);

            //var_dump($file);

            //$type = File::mimeType($event->banner_mime);

            $response = Response::make($file, 200);

            $response->header("Content-Type", $speaker->banner_mime);
            return $response;
            //return response()->download($pathToFile, $fileUpload->original_filename, $headers);

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function getSessionFile(Request $request, $sessionId)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $session = $this->eventRepository->findEventSessionById($sessionId);
            $pathToFile = storage_path("app/public/" . $session->file_path);
            $headers = array(
                'Content-Type: ' . $session->file_mime,
            );
            $file = file_get_contents($pathToFile);

            //var_dump($file);

            //$type = File::mimeType($event->banner_mime);

            $response = Response::make($file, 200);

            $response->header("Content-Type", $session->file_mime);
            return $response;
            //return response()->download($pathToFile, $fileUpload->original_filename, $headers);

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }

    public function getDocumentOrder(Request $request, $id)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $ticketOrder = $this->eventRepository->findEventTicketOrderById($id);
            $pathToFile = storage_path("app/public/" . $ticketOrder->document_path);
            $headers = array(
                'Content-Type: ' . $ticketOrder->banner_mime,
            );
            $file = file_get_contents($pathToFile);

            $response = Response::make($file, 200);

            $response->header("Content-Type", $ticketOrder->banner_mime);
            return $response;

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }

    public function skleton(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'periode' => 'required|date',
                'filter' => 'present',
                'orderBy' => 'required',
                'orderByType' => 'required',
                'limit' => 'required|numeric',
                'offset' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $this->setCreateUserId($inputJson, Auth::id());

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
