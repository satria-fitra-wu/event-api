<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\ComboConstant\FindComboConstantItemById;
use App\Repository\Event\FindEventByCode;
use App\Repository\Event\GetEventDateByEventId;
use App\Repository\Event\GetEventTicketByEventId;
use App\Repository\Event\GetLocationListByEventId;
use App\Repository\UserOrganizer\FindUserOrganizerById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class GetDetailEventController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_code' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();
            $findComboConstantItemById = FindComboConstantItemById::getInstance();
            $getEventDateByEventId = GetEventDateByEventId::getInstance();
            $getEventTicketByEventId = GetEventTicketByEventId::getInstance();
            $findEventByCode = FindEventByCode::getInstance();
            $getLocationListByEventId = GetLocationListByEventId::getInstance();
            $findUserOrganizerById = FindUserOrganizerById::getInstance();

            $event = $findEventByCode->execute($inputJson['event_code']);
            $userOrganizer = $findUserOrganizerById->execute($event->create_user_id);
            $locationList = $getLocationListByEventId->execute($event->event_id);

            $eventDateList = $getEventDateByEventId->execute($event->event_id);
            $ticketList = $getEventTicketByEventId->execute($event->event_id);
            $comboConstant = $findComboConstantItemById->execute($event->event_category);
            $location_type = "";

            $eventDateTobeShowList = [];
            foreach ($eventDateList as $eventDate) {
                $date["event_date"] = date('d-m-Y', strtotime($eventDate->event_date));
                $date["time_from"] = substr($eventDate->time_from, 0, 5);
                $date["time_to"] = substr($eventDate->time_to, 0, 5);
                $date["remark"] = $eventDate->remark;

                $eventDateTobeShowList[] = $date;
                # code...
            }
            foreach ($locationList as $location) {
                if ($location->location_type == '0') {
                    if ($location_type == 'ONLINE') {
                        $location_type = 'ONLINE AND OFFLINE';
                    }
                    if ($location_type == '') {
                        $location_type = 'OFFLINE';
                    }

                }
                if ($location->location_type == '1') {
                    if ($location_type == 'OFFLINE') {
                        $location_type = 'ONLINE AND OFFLINE';
                    }
                    if ($location_type == '') {
                        $location_type = 'ONLINE';
                    }

                }
            }

            $result = [
                "event_category" => $comboConstant->combo_constant_item_name,
                "event_code" => $event->event_code,
                "event_name" => $event->event_name,
                "event_description" => $event->event_description,
                "dateList" => $eventDateTobeShowList,
                "ticketList" => $ticketList,
                "platform_fee" => 0,
                "organizer_name" => $userOrganizer->organizer_name,
                // "date_from" => date('d-m-Y', strtotime($event->date_from)),
                // "date_to" => date('d-m-Y', strtotime($event->date_to)),
                // "time_from" => $event->time_from,
                // "time_to" => $event->time_from,
                "banner_url" => $this->getUrl("getBannerEvent/" . $event->event_code),
                'location_type' => $location_type,
            ];
            return ResponseJson::success($result);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }

    }
}
