<?php

namespace App\Http\Controllers\Event;

use App\Events\OrderTicketEvent;
use App\Http\Controllers\Controller;
use App\Repository\Event\AddEventTicketOrder;
use App\Repository\Event\FindEventById;
use App\Repository\Event\FindEventTicketById;
use App\Repository\Event\GetLastTicketOrderLineNo;
use App\Repository\Event\GetLocationListByEventId;
use App\Repository\Event\IsExistsEventTicketOrderByticketIdAndUserId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class OrderTicketController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_ticket_id' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $user = Auth::user();
            $dateTimeNow = $this->getDatetimeNow();
            $dateNow = $this->getDateNow();
            $isExistsEventTicketOrderByticketIdAndUserId = IsExistsEventTicketOrderByticketIdAndUserId::getInstance();
            $findEventTicketById = FindEventTicketById::getInstance();
            $ticket = $findEventTicketById->execute($inputJson['event_ticket_id']);

            $isExistEventTicketOrder = $isExistsEventTicketOrderByticketIdAndUserId->execute($ticket->event_ticket_id, $user->user_id);

            if ($isExistEventTicketOrder->exists) {
                throw new CoreException(YOU_ALREADY_BUY_THIS_TICKET);

            }

            if (!(strtotime($dateNow) >= strtotime($ticket->date_from) &&
                strtotime($dateNow) <= strtotime($ticket->date_to))) {
                throw new CoreException(PERIODE_TICKET_SALES_END, ["0" => $ticket->ticket_name]);
            }

            if ($ticket->quota != -99 && ($ticket->quota < $ticket->total_order)) {
                throw new CoreException(TICKET_SOLD_OUT);
            }

            $findEventById = FindEventById::getInstance();
            $getLastTicketOrderLineNo = GetLastTicketOrderLineNo::getInstance();
            $addEventTicketOrder = AddEventTicketOrder::getInstance();

            $event = $findEventById->execute($ticket->event_id);

            $orderStatus = ORDER_STATUS_WAITING_PAYMENT;
            $orderStatusCode = ORDER_STATUS_WAITING_PAYMENT_CODE;
            if ($ticket->ticket_price == 0) {
                $orderStatus = ORDER_STATUS_VERIFIED;
                $orderStatusCode = ORDER_STATUS_VERIFIED_CODE;
            }

            $lineNo = $getLastTicketOrderLineNo->execute($ticket->event_ticket_id) + 1;
            $dataInput = [
                "event_id" => $ticket->event_id,
                "event_ticket_id" => $ticket->event_ticket_id,
                "user_id_as_buyer" => true,
                "order_code" => $event->event_code . "-" . $ticket->event_ticket_id . "-" . $lineNo,
                "line_no" => $lineNo,
                "order_user_id" => $user->user_id,
                "name" => $user->full_name,
                "email" => $user->email,
                "instance" => "",
                "phone" => $user->phone,
                "address" => $user->address,
                "payment_method" => PAYMENT_METHOD_MANUAL,
                "order_status" => $orderStatus,
                "order_status_code" => $orderStatusCode,
                "document_path" => "",
                "document_mime" => "",
                "order_price" => $ticket->ticket_price,
                "remark" => "",
            ];
            $dataInput = $this->setCreateUserId($dataInput, $user->user_id);
            $result = $addEventTicketOrder->execute($dataInput);
            if ($result->order_status_code == ORDER_STATUS_VERIFIED_CODE) {
                $getLocationListByEventId = GetLocationListByEventId::getInstance();

                $locationList = $getLocationListByEventId->execute($event->event_id);
                $locationArray = [];
                foreach ($locationList as $location) {
                    $loc = ["location_name" => $location->location_name,
                        "location_url" => $location->location_url,
                        "location_type" => $location->location_type,
                        "location_description" => $location->location_description,
                    ];
                    $locationArray[] = $loc;
                }
                event(new OrderTicketEvent(["locations" => $locationArray, "email" => $result->email]));

            }

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }
}
