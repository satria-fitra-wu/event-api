<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventById;
use App\Repository\Event\GetEventCertificateParameterList;
use App\Repository\Event\GetEventDateListByEventId;
use App\Repository\Event\GetEventLocationListByEventId;
use App\Repository\Event\GetEventSessionListByEventId;
use App\Repository\Event\GetEventSpeakerListByEventId;
use App\Repository\Event\GetEventTicketListByEventId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class FindEventByIdController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_id' => 'required|numeric',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();
            $findEventById = FindEventById::getInstance();
            $getEventDateListByEventId = GetEventDateListByEventId::getInstance();
            $getEventSpeakerListByEventId = GetEventSpeakerListByEventId::getInstance();
            $getEventTicketListByEventId = GetEventTicketListByEventId::getInstance();
            $getEventLocationListByEventId = GetEventLocationListByEventId::getInstance();
            $getEventSessionListByEventId = GetEventSessionListByEventId::getInstance();
            $getEventCertificateParameterList = GetEventCertificateParameterList::getInstance();

            $event = $findEventById->execute($inputJson['event_id']);
            $speakerList = $getEventSpeakerListByEventId->execute($inputJson['event_id']);
            foreach ($speakerList as $speaker) {
                if ($speaker->banner_path != EMPTY_STRING) {
                    $speaker->photo_url = $this->getUrl("/getSpeakerPhoto/" . $speaker->event_speaker_id);
                }
            }

            $sessionList = $getEventSessionListByEventId->execute($inputJson['event_id']);
            foreach ($sessionList as $session) {
                $session->file_url = "";
                if (EMPTY_STRING != $session->file_path) {
                    $session->file_url = $this->getUrl('api/getSessionFile/' . $session->event_session_id);
                }
            }

            $result = [
                "event_id" => $event->event_id,
                "banner_url" => $this->getUrl("getBannerEvent/" . $event->event_code),
                "event_code" => $event->event_code,
                "event_name" => $event->event_name,
                "template_certificate_name" => $event->template_certificate_name,
                "event_category" => $event->event_category,
                "event_description" => $event->event_description,
                "course_url" => $event->course_url,
                "date_from" => date('d-m-Y', strtotime($event->date_from)),
                "date_to" => date('d-m-Y', strtotime($event->date_to)),
                "time_from" => date('h:i', strtotime($event->time_from)),
                "time_to" => date('h:i', strtotime($event->time_to)),
                "eventDateList" => $getEventDateListByEventId->execute($inputJson['event_id']),
                'speakerList' => $speakerList,
                'ticketList' => $getEventTicketListByEventId->execute($inputJson['event_id']),
                'locationList' => $getEventLocationListByEventId->execute($inputJson['event_id']),
                "certificateParameterList" => $getEventCertificateParameterList->execute($inputJson["event_id"]),

                'sessionList' => $sessionList,
            ];
            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }
}
