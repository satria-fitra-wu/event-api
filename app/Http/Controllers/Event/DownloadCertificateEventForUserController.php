<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventById;
use App\Repository\Event\FindEventTicketOrderById;
use App\Repository\Event\GetEventCertificateParameterList;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use setasign\Fpdi\Fpdi;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class DownloadCertificateEventForUserController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_ticket_order_id" => "required",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $findEventById = FindEventById::getInstance();
            $findEventTicketOrderById = FindEventTicketOrderById::getInstance();
            $getEventCertificateParameterList = GetEventCertificateParameterList::getInstance();
            $pdf = new Fpdi();
            $eventTicketOrder = $findEventTicketOrderById->execute($inputJson['event_ticket_order_id']);
            if (Auth::id() != $eventTicketOrder->create_user_id) {
                throw new CoreException(YOU_ARE_NOT_PERMIT_TO_DOWNLOAD_CERTIFICATE);
            }
            $event = $findEventById->execute($eventTicketOrder->event_id);
            $eventCertificateParameterList = $getEventCertificateParameterList->execute($eventTicketOrder->event_id);

            $parameterDefaultValue = ["name" => Auth::user()->full_name];
            $parameterDefaultValue["certificateno"] = $eventTicketOrder->order_code;

            /* <Virtual loop> */
            $pdf->AddPage();
            $pdf->setSourceFile(storage_path('app/public/' . $event->template_certificate_path));
            $tplIdx = $pdf->importPage(1);

            $pdf->useTemplate($tplIdx, 10, 10, 200);

            // now write some text above the imported page
            $pdf->SetFont('Arial');
            //$pdf->SetTextColor(255, 0, 0);

            $pdf->SetRightMargin(30);

            foreach ($eventCertificateParameterList as $eventCertificateParameter) {
                $pdf->SetFontSize($eventCertificateParameter->font_size);

                $pdf->SetXY($eventCertificateParameter->position_x, $eventCertificateParameter->position_y);
                $pdf->SetFontSize($eventCertificateParameter->font_size);
                if (!is_null($eventCertificateParameter->parameter_value) || $eventCertificateParameter->parameter_value != "") {
                    //var_dump($parameterDefaultValue);
                    if (array_key_exists(preg_replace("/(\{{2})(\w+)(\}{2})/i", "$2", $eventCertificateParameter->parameter_value), $parameterDefaultValue)) {
                        //echo "masuk";
                        $pdf->Cell($eventCertificateParameter->width, $eventCertificateParameter->height, $parameterDefaultValue[preg_replace("/(\{{2})(\w+)(\}{2})/i", "$2", $eventCertificateParameter->parameter_value)], 0, 0, $eventCertificateParameter->align);
                    }

                } else {
                    $pdf->Cell(0, 0, $eventCertificateParameter->parameter_value);
                }
            }

            $pdf->Output('D', 'certificate' . $eventTicketOrder->order_code . now() . '.pdf');

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
