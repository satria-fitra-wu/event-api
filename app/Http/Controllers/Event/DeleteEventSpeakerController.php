<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventById;
use App\Repository\Event\FindEventSpeakerById;
use App\Repository\Event\GetEventSpeakerListByEventId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class DeleteEventSpeakerController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_speaker_id" => "required",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $findEventSpeakereById = FindEventSpeakerById::getInstance();
            $findEventById = FindEventById::getInstance();
            $getEventSpeakerListByEventId = GetEventSpeakerListByEventId::getInstance();

            $eventSpeaker = $findEventSpeakereById->execute($inputJson["event_speaker_id"]);
            $event = $findEventById->execute($eventSpeaker->event_id);
            $eventSpeakerList = $getEventSpeakerListByEventId->execute($eventSpeaker->event_id);

            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            if (count($eventSpeakerList) == 1) {
                //throw new CoreException(MIN_ONE_DATE_FILLED);
            }

            $eventSpeaker->delete();
            $resultList = $getEventSpeakerListByEventId->execute($eventSpeaker->event_id);
            foreach ($resultList as $speaker) {
                if ($speaker->banner_path != EMPTY_STRING) {
                    $speaker->photo_url = $this->getUrl("/getSpeakerPhoto/" . $speaker->event_speaker_id);
                }
            }
            return ResponseJson::success($resultList);

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
