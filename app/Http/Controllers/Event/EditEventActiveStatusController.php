<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class EditEventActiveStatusController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_id' => 'required',
                'active' => Rule::in(FLAG_YES, FLAG_NO),
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            // if (Auth::user()->tenant_id == -99) {
            //     throw new CoreException(YOUR_ACCOUNT_FORBID_CREATE_EVENT);
            // }

            $findEventById = FindEventById::getInstance();
            //$this->eventRepository->findEventLocationById($inputJson["event_id"]);
            $event = $findEventById->execute($inputJson["event_id"]);

            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }
            $event->active = $inputJson['active'];

            $event = $this->setCreateUserId($event, Auth::id());
            $event->update();
            return ResponseJson::success($event);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }

    }
}
