<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\AddEventLocation;
use App\Repository\Event\FindEventById;
use App\Repository\Event\GetEventLocationListByEventId;
use App\Repository\Event\GetLastLineNoEventLocationByEventId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class AddEventLocationController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $rules = [
                // 'tenantId' => 'required',
                'event_id' => 'required|numeric',
                'location_type' => 'required',
                'location_name' => 'required',
                'location_description' => 'present',
            ];
            if ($inputJson['location_type'] == "1") {
                $rules['location_url'] = 'required';
            } else {
                $inputJson['location_url'] = "";
            }
            $validator = Validator::make($inputJson, $rules, [
                'required' => 'Kolom :attribute harus diisi.',
            ]);
            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $getLastLineNoEventLocationByEventId = GetLastLineNoEventLocationByEventId::getInstance();
            $addEventLocation = AddEventLocation::getInstance();
            $getEventLocationListByEventId = GetEventLocationListByEventId::getInstance();

            $findEventById = FindEventById::getInstance();
            $event = $findEventById->execute($inputJson["event_id"]);
            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            $inputJson["line_no"] = $getLastLineNoEventLocationByEventId->execute($inputJson["event_id"]) + 1;
            $inputJson['location_description'] = is_null($inputJson['location_description']) ? '' : $inputJson['location_description'];
            $inputJson = $this->setCreateUserId($inputJson, Auth::id());

            $addEventLocation->execute($inputJson);
            $resultList = $getEventLocationListByEventId->execute($inputJson['event_id']);

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }
}
