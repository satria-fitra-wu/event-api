<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventById;
use App\Repository\Event\FindEventSpeakerById;
use App\Repository\Event\GetEventSpeakerListByEventId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class EditEventSpeakerController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //

        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_speaker_id' => 'required|numeric',
                'speaker_name' => 'required',
                'speaker_description' => 'required',
                'flg_speaker_photo' => 'required|numeric',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);
            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            if ($inputJson["flg_speaker_photo"] == "1") {
                $filename_uuid = (string) Str::orderedUuid();
                $extension = $request->speaker_photo->extension();
                $mime = $request->speaker_photo->getClientMimeType();

                $inputJson["file_photo"] = $request->speaker_photo;
                // var_dump(SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension);
                $inputJson["banner_path"] = SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension;

                $inputJson["banner_mime"] = $mime;
                $rules['file_photo'] = 'file|image|mimes:jpeg,png,jpg|max:512';

                $validator = Validator::make($inputJson, $rules, [
                    'required' => 'Kolom :attribute harus diisi.',
                ]);
                if ($validator->fails()) {
                    throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
                }
            } else if ($inputJson["flg_speaker_photo"] == "-99") {

                //$inputJson["banner_path"] = "";
                //$inputJson["banner_mime"] = "";
            }
            $findSpeakerById = FindEventSpeakerById::getInstance();
            $findEventById = FindEventById::getInstance();
            $speaker = $findSpeakerById->execute($inputJson["event_speaker_id"]);
            $event = $findEventById->execute($speaker->event_id);

            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }
            $oldBannerPath = $speaker->banner_path;
            $speaker->speaker_name = $inputJson["speaker_name"];
            $speaker->speaker_description = $inputJson["speaker_description"];
            $speaker->banner_path = is_null($inputJson["banner_path"]) ? "" : $inputJson["banner_path"];
            $speaker->banner_mime = is_null($inputJson["banner_mime"]) ? "" : $inputJson["banner_mime"];
            $speaker = $this->setUdateUserId($speaker, Auth::id());
            $speaker->update();

            if ($inputJson["flg_speaker_photo"] == "1") {

                if (!is_null($oldBannerPath) && $oldBannerPath != "" && file_exists(storage_path('app/public/' . $oldBannerPath))) {
                    unlink(storage_path('app/public') . $oldBannerPath);
                }
                $request->speaker_photo->storeAs('', $inputJson["banner_path"], 'public');
            }
            $getSpeakerListByEventId = GetEventSpeakerListByEventId::getInstance();
            $resultList = $getSpeakerListByEventId->execute($speaker->event_id);
            foreach ($resultList as $speaker) {
                if ($speaker->banner_path != EMPTY_STRING) {
                    $speaker->photo_url = $this->getUrl("/getSpeakerPhoto/" . $speaker->event_speaker_id);
                }
            }
            return ResponseJson::success($resultList);

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
