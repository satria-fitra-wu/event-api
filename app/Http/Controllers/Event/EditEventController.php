<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class EditEventController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $inputJson['banner'] = $request->banner;
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_id' => 'required',
                'event_name' => 'required',
                "event_category" => 'required',

                'event_description' => 'required',
                'course_url' => "present",
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);
            if (!is_null($request->banner)) {
                $validator['banner'] = 'file|image|mimes:jpeg,png,jpg|max:512';
            }

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            // if (Auth::user()->tenant_id == -99) {
            //     throw new CoreException(YOUR_ACCOUNT_FORBID_CREATE_EVENT);
            // }

            $findEventById = FindEventById::getInstance();
            //$this->eventRepository->findEventLocationById($inputJson["event_id"]);
            $event = $findEventById->execute($inputJson["event_id"]);

            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            $event->event_name = $inputJson['event_name'];
            $event->event_category = $inputJson['event_category'];
            $event->event_description = $inputJson['event_description'];
            $event->course_url = is_null($inputJson['course_url']) ? '' : $inputJson['course_url'];

            if (!is_null($request->banner)) {
                if ($event->banner_path != EMPTY_STRING && file_exists(storage_path('app/public/' . $event->banner_path))) {
                    unlink(storage_path('app/public/' . $event->banner_path));
                }
                $filename_uuid = (string) Str::orderedUuid();
                $extension = $request->banner->extension();
                $mime = $request->banner->getClientMimeType();
                $inputJson["banner_path"] = EVENT_BANNER_PATH . '/' . $filename_uuid . '.' . $extension;
                $inputJson["banner_mime"] = $mime;
                $event->banner_path = $inputJson['banner_path'];
                $event->banner_mime = $inputJson['banner_mime'];
                $request->banner->storeAs('', $inputJson["banner_path"], 'public');
            }

            $event = $this->setCreateUserId($event, Auth::id());
            $event->update();
            return ResponseJson::success($event);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }

    }
}
