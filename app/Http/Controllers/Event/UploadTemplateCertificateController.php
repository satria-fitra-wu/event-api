<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class UploadTemplateCertificateController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_id" => "required",
                "file_upload" => "file|image|mimes:jpg,jpeg,png|max:512",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $filename_uuid = (string) Str::orderedUuid();
            $extension = $request->file_upload->extension();
            $mime = $request->file_upload->getClientMimeType();

            $findEventById = FindEventById::getInstance();

            $event = $findEventById->execute($inputJson["event_id"]);
            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            $event->template_certificate_path = TEMPLATE_CERTIFICATE_PATH . "/" . $filename_uuid . "." . $extension;
            $event->template_certificate_name = $filename_uuid . "." . $extension;
            $event->template_certificate_mime = $mime;

            $event->save();
            $request->file_upload->storeAs('', $event->template_certificate_path, 'public');

            return ResponseJson::success($event);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
