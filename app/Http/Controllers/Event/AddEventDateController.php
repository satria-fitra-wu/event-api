<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\AddEventDate;
use App\Repository\Event\FindEventById;
use App\Repository\Event\IsExistEventDateByEventIdAndDate;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class AddEventDateController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_id" => "required",
                "event_date" => "required|date",
                "time_from" => "required|date_format:H:i",
                "time_to" => "required|date_format:H:i",
                "remark" => "present",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $findEventById = FindEventById::getInstance();
            $isExistEventDateByEventIdAndDate = IsExistEventDateByEventIdAndDate::getInstance();

            $isExistsEventDate = $isExistEventDateByEventIdAndDate->execute($inputJson["event_id"], $inputJson["event_date"]);

            $event = $findEventById->execute($inputJson["event_id"]);
            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            if ($isExistsEventDate->exists) {
                //throw new CoreException(DUPLICATE_EVENT_DATE);

            }
            $timeFrom = date_create_from_format("Y-m-d H:i:s", $inputJson["event_date"] . ' ' . $inputJson["time_from"] . ':00');
            $timeto = date_create_from_format("Y-m-d H:i:s", $inputJson["event_date"] . ' ' . $inputJson["time_to"] . ':00');

            if ($timeFrom > $timeto) {
                throw new CoreException(TIME_FROM_MUST_LESS_THAN_TIME_TO);
            }

            $addEventDate = AddEventDate::getInstance();

            $result = $addEventDate->execute($inputJson);

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
