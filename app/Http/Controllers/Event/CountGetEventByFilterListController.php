<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\CountGetEventByFilterList;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class CountGetEventByFilterListController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_date" => "present",
                "event_category_id" => "required",
                "event_ticket_category" => "required",
                "event_location_category" => "required",
                "organizer_name" => "present",
                "keyword" => "present",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $countGetEventByFilterList = CountGetEventByFilterList::getInstance();
            //var_dump($inputJson);
            $result = $countGetEventByFilterList->execute($inputJson["event_date"], $inputJson["event_category_id"], $inputJson["event_ticket_category"],
                $inputJson["event_location_category"], $inputJson["organizer_name"], $inputJson["keyword"]);

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
