<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\GetEventByFilterList;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class GetEventByFilterListController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_date" => "present",
                "event_category_id" => "required",
                "event_ticket_category" => "required",
                "event_location_category" => "required",
                "organizer_name" => "present",
                "keyword" => "present",
                'orderBy' => 'present',
                'orderByType' => 'required',
                'limit' => 'required|numeric',
                'offset' => 'required|numeric',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $getEventByFilterList = GetEventByFilterList::getInstance();

            //var_dump($inputJson);
            $resultList = $getEventByFilterList->execute($inputJson["event_date"], $inputJson["event_category_id"], $inputJson["event_ticket_category"],
                $inputJson["event_location_category"], $inputJson["organizer_name"], $inputJson["keyword"],
                $inputJson["orderBy"], $inputJson["orderByType"], $inputJson["limit"], $inputJson["offset"]);

            foreach ($resultList as $result) {
                $result->flg_free = FLAG_NO;
                if (DB::table('event_ticket')
                    ->where('event_id', $result->event_id)
                    ->where("ticket_price", 0)
                    ->exists()) {
                    $result->flg_free = FLAG_YES;

                }
                $result->event_date = date('d-m-Y', strtotime($result->event_date));
                $result->date_from = date('d-m-Y', strtotime($result->date_from));
                $result->date_to = date('d-m-Y', strtotime($result->date_to));
                $result->event_description = substr(strip_tags($result->event_description), 0, 25);
                $result->banner_url = $this->getUrl("getBannerEvent/" . $result->event_code);
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
