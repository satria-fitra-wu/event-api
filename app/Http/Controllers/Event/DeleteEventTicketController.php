<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventById;
use App\Repository\Event\FindEventTicketById;
use App\Repository\Event\GetEventTicketListByEventId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class DeleteEventTicketController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_ticket_id" => "required",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $findEventTicketById = FindEventTicketById::getInstance();
            $findEventById = FindEventById::getInstance();
            $getEventTicketListByEventId = GetEventTicketListByEventId::getInstance();

            $eventTicket = $findEventTicketById->execute($inputJson["event_ticket_id"]);
            $event = $findEventById->execute($eventTicket->event_id);
            $eventTicketList = $getEventTicketListByEventId->execute($eventTicket->event_id);

            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            if (count($eventTicketList) == 1) {
                throw new CoreException(MIN_ONE_TICKET_FILLED);
            }

            $eventTicket->delete();
            $resultList = $getEventTicketListByEventId->execute($eventTicket->event_id);

            return ResponseJson::success($resultList);

        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
