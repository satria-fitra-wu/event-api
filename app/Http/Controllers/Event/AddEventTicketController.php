<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\AddEventTicket;
use App\Repository\Event\FindEventById;
use App\Repository\Event\GetEventTicketListByEventId;
use App\Repository\Event\GetLastLineNoEventTicketByEventId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class AddEventTicketController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_id' => 'required|numeric',
                'ticket_name' => 'required',
                'ticket_price' => 'required|numeric',
                'date_from' => 'required|date',
                'date_to' => 'required|date',
                'quota' => 'required|numeric',
                'ticket_description' => 'present',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);
            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            if (strtotime($inputJson['date_from']) > strtotime($inputJson['date_to'])) {
                throw new CoreException(DATE_FROM_MUST_BE_LESS_OR_EQUALS_WITH_DATE_TO, [
                    "0" => Date('d-m-Y', strtotime($inputJson['date_from'])),
                    "1" => Date('d-m-Y', strtotime($inputJson['date_to'])),
                ]);
            }
            if ($inputJson['quota'] < 0 && $inputJson['quota'] != -99) {
                throw new CoreException(TICKET_QUOTA_MUST_BE_GREATER_THAN_ZERO);
            }

            if ($inputJson['ticket_price'] < 0) {
                throw new CoreException(TICKET_PRICE_MUST_BE_GREATER_THAN_ZERO);
            }
            $getLastLineNoEventTicketByEventId = GetLastLineNoEventTicketByEventId::getInstance();
            $addEventTicket = AddEventTicket::getInstance();
            $getEventTicketListByEventId = GetEventTicketListByEventId::getInstance();

            $findEventById = FindEventById::getInstance();

            $event = $findEventById->execute($inputJson["event_id"]);
            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            $inputJson["line_no"] = $getLastLineNoEventTicketByEventId->execute($inputJson["event_id"]) + 1;
            $inputJson['ticket_description'] = is_null($inputJson['ticket_description']) ? '' : $inputJson['ticket_description'];
            $inputJson = $this->setCreateUserId($inputJson, Auth::id());

            $addEventTicket->execute($inputJson);
            $resultList = $getEventTicketListByEventId->execute($inputJson['event_id']);
            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }
}
