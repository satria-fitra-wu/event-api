<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\FindEventTicketOrderById;
use App\Repository\Event\GetEventLocationListByEventTicketOrderId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class GetEventLocationListByEventTicketOrderIdController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_ticket_order_id" => "required",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $findEventTicketOrderById = FindEventTicketOrderById::getInstance();
            $eventTicketOrder = $findEventTicketOrderById->execute($inputJson["event_ticket_order_id"]);

            if ($eventTicketOrder->order_status != ORDER_STATUS_VERIFIED) {
                throw new CoreException("MOHON TIKET DIBAYAR TERLEBIH DAHULU");
            }
            $getEventLocationListByEventTicketOrderId = GetEventLocationListByEventTicketOrderId::getInstance();

            $result = $getEventLocationListByEventTicketOrderId->execute($inputJson["event_ticket_order_id"]);

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
