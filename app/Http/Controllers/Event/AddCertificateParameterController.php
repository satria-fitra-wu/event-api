<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\AddCertificateParameter;
use App\Repository\Event\FindEventById;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class AddCertificateParameterController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                "event_id" => "required",
                "parameter_name" => "required",
                //"parameter_type" => "required",
                "parameter_value" => "required",
                "position_x" => "required|numeric|min:0",
                "position_y" => "required|numeric|min:0",
                "width" => "required|numeric|min:0",
                "height" => "required|numeric|min:0",
                "font_size" => "required|numeric|min:0",
                "align" => ["required", Rule::in(["L", "C", "R"])],
                //"default_value" => "required",
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            $addCertificateParameter = AddCertificateParameter::getInstance();
            $findEventById = FindEventById::getInstance();
            $event = $findEventById->execute($inputJson["event_id"]);
            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            $certificateParameter = ["event_id" => $event->event_id,
                "parameter_name" => $inputJson["parameter_name"],
                "parameter_type" => "",
                "parameter_value" => $inputJson["parameter_value"],
                "default_value" => "",
                "position_x" => $inputJson["position_x"],
                "position_y" => $inputJson["position_y"],
                "width" => $inputJson["width"],
                "height" => $inputJson["height"],
                "font_size" => $inputJson["font_size"],
                "align" => $inputJson["align"]];
            $result = $addCertificateParameter->execute($certificateParameter);

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
}
