<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\AddEvent;
use App\Repository\Event\ValEventExistsByCode;
use App\Repository\UserOrganizer\FindUserOrganizerById;
use Exception;
use function PHPUnit\Framework\isNull;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class AddEventController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $inputJson['banner'] = $request->banner;
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                //'event_code' => 'required',
                'event_name' => 'required',
                "event_category" => 'required',
                // 'date_from' => 'required|date',
                // 'date_to' => 'required|date',
                // 'time_from' => 'required|date_format:H:i',
                // 'time_to' => 'required|date_format:H:i',
                'event_description' => 'required',
                'course_url' => 'present',
                'banner' => 'file|image|mimes:jpeg,png,jpg|max:512',
                'dateList' => 'present',
                'speakerList' => 'present',
                'ticketList' => 'present',
                'locationList' => 'present',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }
            if (Auth::user()->tenant_id == -99) {
                throw new CoreException(YOUR_ACCOUNT_FORBID_CREATE_EVENT);
            }
            $valEventExistByCode = ValEventExistsByCode::getInstance();
            $findUserOrganizerById = FindUserOrganizerById::getInstance();

            $userOrganizer = $findUserOrganizerById->execute(Auth::id());
            $valEventExistByCode->execute($inputJson['event_code'], Auth::user()->tenant_id);

            // if (strtotime($inputJson['date_from']) > strtotime($inputJson['date_to'])) {
            //     throw new CoreException(DATE_FROM_MUST_BE_LESS_OR_EQUALS_WITH_DATE_TO, [
            //         "0" => Date('d-m-Y', strtotime($inputJson['date_from'])),
            //         "1" => Date('d-m-Y', strtotime($inputJson['date_to'])),
            //     ]);
            // }

            $dateList = json_decode($inputJson['dateList'], true);
            $eventDateList = [];
            $dateListInput = [];
            foreach ($dateList as $eventDate) {
                $rules = [
                    "event_date" => "required|date",
                    "time_from" => "required|date_format:H:i",
                    "time_to" => "required|date_format:H:i",
                    "remark" => "present",
                ];
                $validator = Validator::make($eventDate, $rules, [
                    'required' => 'Kolom :attribute harus diisi.',
                ]);

                if ($validator->fails()) {
                    throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
                }
                if (in_array($eventDate["event_date"], $eventDateList)) {
                    //throw new CoreException(DUPLICATE_EVENT_DATE);
                } else {
                    $eventDateList[] = $eventDate["event_date"];
                }

                $timeFrom = date_create_from_format("Y-m-d H:i:s", $eventDate["event_date"] . ' ' . $eventDate["time_from"] . ':00');
                $timeto = date_create_from_format("Y-m-d H:i:s", $eventDate["event_date"] . ' ' . $eventDate["time_to"] . ':00');

                if ($timeFrom > $timeto) {
                    throw new CoreException(TIME_FROM_MUST_LESS_THAN_TIME_TO);
                }
                $eventDate["remark"] = isNull($eventDate["remark"]) ? "" : $eventDate["remark"];
                $eventDate = $this->setCreateUserId($eventDate, Auth::id());
                $dateListInput[] = $eventDate;
                # code...
            }

            if (count($dateListInput) == 0) {
                throw new CoreException(MIN_ONE_DATE_FILLED);
            }

            $lineNoSpeaker = 0;
            $speakerList = json_decode($inputJson['speakerList'], true);
            $speakerListInput = [];
            foreach ($speakerList as $speaker) {
                $rules = [
                    // 'tenantId' => 'required',
                    'speaker_name' => 'required',
                    'speaker_description' => 'required',
                ];
                if ($speaker["flg_speaker_photo"] == "1") {
                    $filename_uuid = (string) Str::orderedUuid();
                    $extension = $request->{"speakerPhoto" . $lineNoSpeaker}->extension();
                    $mime = $request->{"speakerPhoto" . $lineNoSpeaker}->getClientMimeType();

                    $speaker["file_photo"] = $request->{"speakerPhoto" . $lineNoSpeaker};
                    // var_dump(SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension);
                    $speaker["banner_path"] = SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension;

                    $speaker["banner_mime"] = $mime;
                    $rules['file_photo'] = 'file|image|mimes:jpeg,png,jpg|max:512';
                } else {
                    $speaker["banner_path"] = "";
                    $speaker["banner_mime"] = "";

                }
                ;
                // var_dump( $speaker);
                $validator = Validator::make($speaker, $rules, [
                    'required' => 'Kolom :attribute harus diisi.',
                ]);
                if ($validator->fails()) {
                    throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
                }
                $speaker["line_no"] = $lineNoSpeaker + 1;
                $lineNoSpeaker++;
                $speaker = $this->setCreateUserId($speaker, Auth::id());
                $speakerListInput[] = $speaker;
            }

            $lineNoTicket = 0;
            $ticketList = json_decode($inputJson['ticketList'], true);
            if (count($ticketList) == 0) {
                throw new CoreException(MIN_ONE_TICKET_FILLED);
            }

            $ticketListInput = [];
            foreach ($ticketList as $ticket) {
                $rules = [
                    // 'tenantId' => 'required',
                    'date_from' => 'required|date',
                    'date_to' => 'required|date',
                    'quota' => 'required|numeric',
                    'ticket_name' => 'required',
                    'ticket_price' => 'required|numeric',
                    'ticket_description' => 'present',
                ];

                // var_dump( $speaker);
                $validator = Validator::make($ticket, $rules, [
                    'required' => 'Kolom :attribute harus diisi.',
                ]);
                if ($validator->fails()) {
                    throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
                }
                if (strtotime($ticket['date_from']) > strtotime($ticket['date_to'])) {
                    throw new CoreException(DATE_FROM_MUST_BE_LESS_OR_EQUALS_WITH_DATE_TO, [
                        "0" => Date('d-m-Y', strtotime($ticket['date_from'])),
                        "1" => Date('d-m-Y', strtotime($ticket['date_to'])),
                    ]);
                }
                if ($ticket['quota'] < 0 && $ticket['quota'] != -99) {
                    throw new CoreException(TICKET_QUOTA_MUST_BE_GREATER_THAN_ZERO);
                }
                if ($ticket['ticket_price'] < 0) {
                    throw new CoreException(TICKET_PRICE_MUST_BE_GREATER_THAN_ZERO);
                }
                $ticket["line_no"] = $lineNoTicket + 1;
                $ticket['ticket_description'] = is_null($ticket['ticket_description']) ? '' : $ticket['ticket_description'];

                $ticket = $this->setCreateUserId($ticket, Auth::id());
                $ticketListInput[] = $ticket;
                $lineNoTicket++;
            }

            $lineNoLocation = 0;
            $locationList = json_decode($inputJson['locationList'], true);
            if (count($locationList) == 0) {
                throw new CoreException(MIN_ONE_LOCATION_FILLED);
            }

            $locationListInput = [];
            foreach ($locationList as $location) {
                $rules = [
                    // 'tenantId' => 'required',
                    'location_type' => 'required',
                    'location_name' => 'required',
                    'location_description' => 'present',
                ];

                if ($location['location_type'] == "1") {
                    $rules['location_url'] = 'required';
                } else {
                    $location['location_url'] = "";
                }

                // var_dump( $speaker);
                $validator = Validator::make($location, $rules, [
                    'required' => 'Kolom :attribute harus diisi.',
                ]);
                if ($validator->fails()) {
                    throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
                }
                $location["line_no"] = $lineNoLocation + 1;
                $location['location_description'] = is_null($location['location_description']) ? '' : $location['location_description'];

                $location = $this->setCreateUserId($location, Auth::id());
                $lineNoLocation++;
                $locationListInput[] = $location;
            }

            $filename_uuid = (string) Str::orderedUuid();
            $extension = $request->banner->extension();
            $mime = $request->banner->getClientMimeType();

            $inputJson["banner_path"] = EVENT_BANNER_PATH . '/' . $filename_uuid . '.' . $extension;
            $inputJson["banner_mime"] = $mime;
            $inputJson["organizer_user_id"] = Auth::id();

            $eventName = $inputJson["event_name"];
            $eventName = str_replace(' ', '-', $eventName);
            $eventName = preg_replace('/[^A-Za-z0-9\-]/', '', $eventName);

            $inputJson["event_code"] = Str::slug($eventName, '-');
            $inputJson['course_url'] = is_null($inputJson['course_url']) ? '' : $inputJson['course_url'];

            $inputJson = $this->setCreateUserId($inputJson, Auth::id());

            $addEvent = AddEvent::getInstance();
            $result = $addEvent->execute($inputJson, $speakerListInput, $ticketListInput, $locationListInput, $dateListInput);

            $request->banner->storeAs('', $inputJson["banner_path"], 'public');

            $lineNoSpeaker = 0;
            foreach ($speakerListInput as $speaker) {
                if ($speaker["flg_speaker_photo"] == "1") {
                    // $filename_uuid = (string)Str::orderedUuid();
                    // $extension = $request->{"speakerPhoto" . $lineNoSpeaker}->extension();

                    $request->{"speakerPhoto" . $lineNoSpeaker}->storeAs('', $speaker["banner_path"], 'public');
                }
                $lineNoSpeaker++;
            }
            //
            return ResponseJson::success($result);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }

    }
}
