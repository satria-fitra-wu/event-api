<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Repository\Event\AddEventSpeaker;
use App\Repository\Event\FindEventById;
use App\Repository\Event\GetEventSpeakerListByEventId;
use App\Repository\Event\GetLastNoEventSpeakerByEventId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Uinws\CoreApi\Exceptions\CoreException;
use Uinws\CoreApi\Helper\ResponseJson;
use Uinws\CoreApi\Traits\UinWsBaseController;

class AddEventSpeakerController extends Controller
{
    use UinWsBaseController;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_id' => 'required|numeric',
                'speaker_name' => 'required',
                'speaker_description' => 'required',
                'flg_speaker_photo' => 'required|numeric',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);
            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $findEventById = FindEventById::getInstance();
            $event = $findEventById->execute($inputJson["event_id"]);

            if (Auth::id() != $event->create_user_id) {
                throw new CoreException(YOU_CAN_NO_EDIT_THIS_DOCUMENT_YOU_ARE_NOT_THE_OWNER_THIS_DOCUMENT);
            }

            if ($inputJson["flg_speaker_photo"] == "1") {
                $filename_uuid = (string) Str::orderedUuid();
                $extension = $request->speaker_photo->extension();
                $mime = $request->speaker_photo->getClientMimeType();

                $inputJson["file_photo"] = $request->speaker_photo;
                // var_dump(SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension);
                $inputJson["banner_path"] = SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension;

                $inputJson["banner_mime"] = $mime;
                $rules['file_photo'] = 'file|image|mimes:jpeg,png,jpg|max:512';

                $validator = Validator::make($inputJson, $rules, [
                    'required' => 'Kolom :attribute harus diisi.',
                ]);
                if ($validator->fails()) {
                    throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
                }
            } else {
                $inputJson["banner_path"] = "";
                $inputJson["banner_mime"] = "";
            }
            $getLastNoEventSpeakerByEventId = GetLastNoEventSpeakerByEventId::getInstance();
            $getEventSpeakerListByEventId = GetEventSpeakerListByEventId::getInstance();
            $addEventSpeaker = AddEventSpeaker::getInstance();
            $inputJson["line_no"] = $getLastNoEventSpeakerByEventId->execute($inputJson["event_id"]) + 1;

            $inputJson = $this->setCreateUserId($inputJson, Auth::id());

            $addEventSpeaker->execute($inputJson);
            $resultList = $getEventSpeakerListByEventId->execute($inputJson["event_id"]);
            if ($inputJson["flg_speaker_photo"] == "1") {
                $request->speaker_photo->storeAs('', $inputJson["banner_path"], 'public');
            }
            foreach ($resultList as $speaker) {
                if ($speaker->banner_path != EMPTY_STRING) {
                    $speaker->photo_url = $this->getUrl("/getSpeakerPhoto/" . $speaker->event_speaker_id);
                }
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }

    }
}
