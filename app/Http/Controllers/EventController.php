<?php
namespace App\Http\Controllers;

use App\Events\OrderTicketEvent;
use App\Exceptions\CoreException;
use App\Http\Helper\ResponseJson;
use App\Repository\ComboConstant\FindComboConstantItemById;
use App\Repository\EventRepository;
use App\Repository\Event\GetLocationListByEventId;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class EventController extends Controller
{

    //
    public $eventRepository;

    // public $userRepository;
    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository::getInstance();
    }

    public function addEvent(Request $request)
    {

    }

    // public function editEvent(Request $request)
    // {
    //     try {
    //         if ($this->isJson($request->getContent())) {
    //             $inputJson = json_decode($request->getContent(), true);
    //         } else {
    //             $inputJson = $request->input();
    //         }
    //         $inputJson['banner'] = $request->banner;
    //         $validator = Validator::make($inputJson, [
    //             // 'tenantId' => 'required',
    //             'event_id' => 'required',
    //             'event_name' => 'required',
    //             "event_category" => 'required',
    //             'date_from' => 'required|date',
    //             'date_to' => 'required|date',
    //             'time_from' => 'required|date_format:H:i',
    //             'time_to' => 'required|date_format:H:i',
    //             'event_description' => 'required',
    //         ], [
    //             'required' => 'Kolom :attribute harus diisi.',
    //         ]);
    //         if (!is_null($request->banner)) {
    //             $validator['banner'] = 'file|image|mimes:jpeg,png,jpg|max:512';
    //         }

    //         if ($validator->fails()) {
    //             throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //         }
    //         if (Auth::user()->tenant_id == -99) {
    //             throw new CoreException(YOUR_ACCOUNT_FORBID_CREATE_EVENT);
    //         }

    //         if (strtotime($inputJson['date_from']) > strtotime($inputJson['date_to'])) {
    //             throw new CoreException(DATE_FROM_MUST_BE_LESS_OR_EQUALS_WITH_DATE_TO, [
    //                 "0" => Date('d-m-Y', strtotime($inputJson['date_from'])),
    //                 "1" => Date('d-m-Y', strtotime($inputJson['date_to'])),
    //             ]);
    //         }

    //         $event = $this->eventRepository->findEventById($inputJson["event_id"]);
    //         $event->event_name = $inputJson['event_name'];
    //         $event->event_category = $inputJson['event_category'];
    //         $event->date_from = $inputJson['date_from'];
    //         $event->date_to = $inputJson['date_to'];
    //         $event->time_from = $inputJson['time_from'];
    //         $event->time_to = $inputJson['time_to'];
    //         $event->event_description = $inputJson['event_description'];

    //         if (!is_null($request->banner)) {
    //             if ($event->banner_path != EMPTY_STRING && file_exists(storage_path('app/public/' . $event->banner_path))) {
    //                 unlink(storage_path('app/public/' . $event->banner_path));
    //             }
    //             $filename_uuid = (string) Str::orderedUuid();
    //             $extension = $request->banner->extension();
    //             $mime = $request->banner->getClientMimeType();
    //             $inputJson["banner_path"] = EVENT_BANNER_PATH . '/' . $filename_uuid . '.' . $extension;
    //             $inputJson["banner_mime"] = $mime;
    //             $event->banner_path = $inputJson['banner_path'];
    //             $event->banner_mime = $inputJson['banner_mime'];
    //             $request->banner->storeAs('', $inputJson["banner_path"], 'public');
    //         }

    //         $event = $this->setCreateUserId($event, Auth::id());
    //         $event->update();
    //         return ResponseJson::success($event);
    //     } catch (Exception $e) {
    //         return ResponseJson::fail($e);
    //     }
    // }

    // public function addSpeaker(Request $request)
    // {
    //     try {
    //         if ($this->isJson($request->getContent())) {
    //             $inputJson = json_decode($request->getContent(), true);
    //         } else {
    //             $inputJson = $request->input();
    //         }

    //         $validator = Validator::make($inputJson, [
    //             // 'tenantId' => 'required',
    //             'event_id' => 'required|numeric',
    //             'speaker_name' => 'required',
    //             'speaker_description' => 'required',
    //             'flg_speaker_photo' => 'required|numeric',
    //         ], [
    //             'required' => 'Kolom :attribute harus diisi.',
    //         ]);
    //         if ($validator->fails()) {
    //             throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //         }
    //         if ($inputJson["flg_speaker_photo"] == "1") {
    //             $filename_uuid = (string) Str::orderedUuid();
    //             $extension = $request->speaker_photo->extension();
    //             $mime = $request->speaker_photo->getClientMimeType();

    //             $inputJson["file_photo"] = $request->speaker_photo;
    //             // var_dump(SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension);
    //             $inputJson["banner_path"] = SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension;

    //             $inputJson["banner_mime"] = $mime;
    //             $rules['file_photo'] = 'file|image|mimes:jpeg,png,jpg|max:512';

    //             $validator = Validator::make($inputJson, $rules, [
    //                 'required' => 'Kolom :attribute harus diisi.',
    //             ]);
    //             if ($validator->fails()) {
    //                 throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //             }
    //         } else {
    //             $inputJson["banner_path"] = "";
    //             $inputJson["banner_mime"] = "";
    //         }
    //         $inputJson["line_no"] = $this->eventRepository->getLastLineNoSpeaker($inputJson["event_id"]) + 1;

    //         $inputJson = $this->setCreateUserId($inputJson, Auth::id());

    //         $this->eventRepository->addSpeaker($inputJson);
    //         $resultList = $this->eventRepository->getSpeakerListByEventId($inputJson["event_id"]);
    //         if ($inputJson["flg_speaker_photo"] == "1") {
    //             // $filename_uuid = (string)Str::orderedUuid();
    //             // $extension = $request->{"speakerPhoto" . $lineNoSpeaker}->extension();

    //             $request->speaker_photo->storeAs('', $inputJson["banner_path"], 'public');
    //         }
    //         return ResponseJson::success($resultList);
    //     } catch (Exception $e) {
    //         Log::error($e);
    //         return ResponseJson::fail($e);
    //     }
    // }

    // public function editSpeaker(Request $request)
    // {
    //     try {
    //         if ($this->isJson($request->getContent())) {
    //             $inputJson = json_decode($request->getContent(), true);
    //         } else {
    //             $inputJson = $request->input();
    //         }

    //         $validator = Validator::make($inputJson, [
    //             // 'tenantId' => 'required',
    //             'event_speaker_id' => 'required|numeric',
    //             'speaker_name' => 'required',
    //             'speaker_description' => 'required',
    //             'flg_speaker_photo' => 'required|numeric',
    //         ], [
    //             'required' => 'Kolom :attribute harus diisi.',
    //         ]);
    //         if ($validator->fails()) {
    //             throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //         }
    //         if ($inputJson["flg_speaker_photo"] == "1") {
    //             $filename_uuid = (string) Str::orderedUuid();
    //             $extension = $request->speaker_photo->extension();
    //             $mime = $request->speaker_photo->getClientMimeType();

    //             $inputJson["file_photo"] = $request->speaker_photo;
    //             // var_dump(SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension);
    //             $inputJson["banner_path"] = SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension;

    //             $inputJson["banner_mime"] = $mime;
    //             $rules['file_photo'] = 'file|image|mimes:jpeg,png,jpg|max:512';

    //             $validator = Validator::make($inputJson, $rules, [
    //                 'required' => 'Kolom :attribute harus diisi.',
    //             ]);
    //             if ($validator->fails()) {
    //                 throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //             }
    //         } else if ($inputJson["flg_speaker_photo"] == "-99") {

    //             //$inputJson["banner_path"] = "";
    //             //$inputJson["banner_mime"] = "";
    //         }
    //         $speaker = $this->eventRepository->findEventSpeakerById($inputJson["event_speaker_id"]);
    //         $oldBannerPath = $speaker->banner_path;
    //         $speaker->speaker_name = $inputJson["speaker_name"];
    //         $speaker->speaker_description = $inputJson["speaker_description"];
    //         $speaker->banner_path = $inputJson["banner_path"];
    //         $speaker->banner_mime = $inputJson["banner_mime"];
    //         $speaker = $this->setUdateUserId($speaker, Auth::id());
    //         $speaker->update();
    //         if ($inputJson["flg_speaker_photo"] == "1") {

    //             if (!isNull($oldBannerPath) && $oldBannerPath != "" && file_exists(storage_path('app/public/' . $oldBannerPath))) {
    //                 unlink(storage_path('app/public') . $oldBannerPath);
    //             }
    //             $request->speaker_photo->storeAs('', $inputJson["banner_path"], 'public');
    //         }
    //         $resultList = $this->eventRepository->getSpeakerListByEventId($speaker->event_id);
    //         foreach ($resultList as $speaker) {
    //             $speaker->photo_url = $this->getUrl("/getSpeakerPhoto/" . $speaker->event_speaker_id);
    //         }

    //         return ResponseJson::success($resultList);
    //     } catch (Exception $e) {
    //         Log::error($e);
    //         return ResponseJson::fail($e);
    //     }
    // }

    // public function addTicket(Request $request)
    // {
    //     try {
    //         if ($this->isJson($request->getContent())) {
    //             $inputJson = json_decode($request->getContent(), true);
    //         } else {
    //             $inputJson = $request->input();
    //         }

    //         $validator = Validator::make($inputJson, [
    //             // 'tenantId' => 'required',
    //             'event_id' => 'required|numeric',
    //             'ticket_name' => 'required',
    //             'ticket_price' => 'required|numeric',
    //             'date_from' => 'required|date',
    //             'date_to' => 'required|date',
    //             'quota' => 'required|numeric',
    //             'ticket_description' => 'required',
    //         ], [
    //             'required' => 'Kolom :attribute harus diisi.',
    //         ]);
    //         if ($validator->fails()) {
    //             throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //         }

    //         if (strtotime($inputJson['date_from']) > strtotime($inputJson['date_to'])) {
    //             throw new CoreException(DATE_FROM_MUST_BE_LESS_OR_EQUALS_WITH_DATE_TO, [
    //                 "0" => Date('d-m-Y', strtotime($inputJson['date_from'])),
    //                 "1" => Date('d-m-Y', strtotime($inputJson['date_to'])),
    //             ]);
    //         }
    //         if ($inputJson['quota'] < 0 && $inputJson['quota'] != -99) {
    //             throw new CoreException(TICKET_QUOTA_MUST_BE_GREATER_THAN_ZERO);
    //         }

    //         if ($inputJson['ticket_price'] < 0) {
    //             throw new CoreException(TICKET_PRICE_MUST_BE_GREATER_THAN_ZERO);
    //         }
    //         $inputJson["line_no"] = $this->eventRepository->getLastLineNoTicket($inputJson["event_id"]) + 1;

    //         $inputJson = $this->setCreateUserId($inputJson, Auth::id());

    //         $this->eventRepository->addTicket($inputJson);
    //         $resultList = $this->eventRepository->getTicketListByEventId($inputJson["event_id"]);
    //         return ResponseJson::success($resultList);
    //     } catch (Exception $e) {
    //         Log::error($e);
    //         return ResponseJson::fail($e);
    //     }
    // }

    // public function editTicket(Request $request)
    // {
    //     try {
    //         if ($this->isJson($request->getContent())) {
    //             $inputJson = json_decode($request->getContent(), true);
    //         } else {
    //             $inputJson = $request->input();
    //         }

    //         $validator = Validator::make($inputJson, [
    //             // 'tenantId' => 'required',
    //             'event_ticket_id' => 'required|numeric',
    //             'ticket_name' => 'required',
    //             'ticket_price' => 'required|numeric',
    //             'date_from' => 'required|date',
    //             'date_to' => 'required|date',
    //             'quota' => 'required|numeric',
    //             'ticket_description' => 'required',
    //         ], [
    //             'required' => 'Kolom :attribute harus diisi.',
    //         ]);
    //         if ($validator->fails()) {
    //             throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //         }

    //         if (strtotime($inputJson['date_from']) > strtotime($inputJson['date_to'])) {
    //             throw new CoreException(DATE_FROM_MUST_BE_LESS_OR_EQUALS_WITH_DATE_TO, [
    //                 "0" => Date('d-m-Y', strtotime($inputJson['date_from'])),
    //                 "1" => Date('d-m-Y', strtotime($inputJson['date_to'])),
    //             ]);
    //         }
    //         if ($inputJson['quota'] < 0 && $inputJson['quota'] != -99) {
    //             throw new CoreException(TICKET_QUOTA_MUST_BE_GREATER_THAN_ZERO);
    //         }

    //         if ($inputJson['ticket_price'] < 0) {
    //             throw new CoreException(TICKET_PRICE_MUST_BE_GREATER_THAN_ZERO);
    //         }

    //         $ticket = $this->eventRepository->findEventTicketById($inputJson["event_ticket_id"]);
    //         $ticket->ticket_name = $inputJson["ticket_name"];
    //         $ticket->ticket_price = $inputJson["ticket_price"];
    //         $ticket->date_from = $inputJson["date_from"];
    //         $ticket->date_to = $inputJson["date_to"];
    //         $ticket->quota = $inputJson["quota"];
    //         $ticket->ticket_description = $inputJson["ticket_description"];

    //         $ticket = $this->setCreateUserId($ticket, Auth::id());
    //         $ticket->update();
    //         $resultList = $this->eventRepository->getTicketListByEventId($ticket->event_id);
    //         return ResponseJson::success($resultList);
    //     } catch (Exception $e) {
    //         Log::error($e);
    //         return ResponseJson::fail($e);
    //     }
    // }

    // public function addLocation(Request $request)
    // {
    //     try {
    //         if ($this->isJson($request->getContent())) {
    //             $inputJson = json_decode($request->getContent(), true);
    //         } else {
    //             $inputJson = $request->input();
    //         }
    //         $rules = [
    //             // 'tenantId' => 'required',
    //             'event_id' => 'required|numeric',
    //             'location_type' => 'required',
    //             'location_name' => 'required',
    //             'location_description' => 'required',
    //         ];
    //         if ($inputJson['location_type'] == "1") {
    //             $rules['location_url'] = 'required';
    //         } else {
    //             $inputJson['location_url'] = "";
    //         }
    //         $validator = Validator::make($inputJson, $rules, [
    //             'required' => 'Kolom :attribute harus diisi.',
    //         ]);
    //         if ($validator->fails()) {
    //             throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //         }

    //         $inputJson["line_no"] = $this->eventRepository->getLastLineNoLocation($inputJson["event_id"]) + 1;

    //         $inputJson = $this->setCreateUserId($inputJson, Auth::id());

    //         $this->eventRepository->addLocation($inputJson);
    //         $resultList = $this->eventRepository->getLocationListByEventId($inputJson["event_id"]);

    //         return ResponseJson::success($resultList);
    //     } catch (Exception $e) {
    //         Log::error($e);
    //         return ResponseJson::fail($e);
    //     }
    // }

    // public function editLocation(Request $request)
    // {
    //     try {
    //         if ($this->isJson($request->getContent())) {
    //             $inputJson = json_decode($request->getContent(), true);
    //         } else {
    //             $inputJson = $request->input();
    //         }
    //         $rules = [
    //             // 'tenantId' => 'required',
    //             'event_location_id' => 'required|numeric',
    //             'location_type' => 'required',
    //             'location_name' => 'required',
    //             'location_description' => 'required',
    //         ];
    //         if ($inputJson['location_type'] == "1") {
    //             $rules['location_url'] = 'required';
    //         } else {
    //             $inputJson['location_url'] = "";
    //         }
    //         $validator = Validator::make($inputJson, $rules, [
    //             'required' => 'Kolom :attribute harus diisi.',
    //         ]);
    //         if ($validator->fails()) {
    //             throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
    //         }

    //         $location = $this->eventRepository->findEventLocationById($inputJson["event_location_id"]);
    //         $location->location_type = $inputJson["location_type"];
    //         $location->location_name = $inputJson["location_name"];
    //         $location->location_description = $inputJson["location_description"];
    //         $location->location_url = $inputJson["location_url"];
    //         $location = $this->setUdateUserId($location, Auth::id());
    //         $location->update();
    //         $resultList = $this->eventRepository->getLocationListByEventId($location->event_id);
    //         return ResponseJson::success($resultList);
    //     } catch (Exception $e) {
    //         Log::error($e);
    //         return ResponseJson::fail($e);
    //     }
    // }

    public function addSession(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $rules = [
                // 'tenantId' => 'required',
                'event_id' => 'required|numeric',
                'session_name' => 'required',
                'session_description' => 'required',
                'flg_file' => 'required|numeric',
                'ticket_list' => 'required',
            ];
            $validator = Validator::make($inputJson, $rules, [
                'required' => 'Kolom :attribute harus diisi.',
            ]);
            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            if ($inputJson["flg_file"] == "1") {
                $filename_uuid = (string) Str::orderedUuid();
                $extension = $request->session_file->extension();
                $mime = $request->session_file->getClientMimeType();

                $inputJson["session_file"] = $request->session_file;
                // var_dump(SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension);
                $inputJson["file_path"] = SESSION_PATH . "/" . $filename_uuid . "." . $extension;

                $inputJson["file_mime"] = $mime;
                $rules['session_file'] = 'file|mimes:docx,pdf,doc|max:5120';

                $validator = Validator::make($inputJson, $rules, [
                    'required' => 'Kolom :attribute harus diisi.',
                ]);
                if ($validator->fails()) {
                    throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
                }
            } else {
                $inputJson["file_path"] = "";
                $inputJson["file_mime"] = "";
            }

            $inputJson["line_no"] = $this->eventRepository->getLastLineNoSession($inputJson["event_id"]) + 1;

            $inputJson = $this->setCreateUserId($inputJson, Auth::id());
            $sessionTicketList = [];
            foreach ($inputJson["ticket_list"] as $ticket) {
                $sessionTicket = ["event_ticket_id" => $ticket];
                $sessionTicket = $this->setCreateUserId($sessionTicket, Auth::id());
                $sessionTicketList[] = $sessionTicket;
            }
            $inputJson["sessionTicketList"] = $sessionTicketList;

            $this->eventRepository->addSession($inputJson);
            $resultList = $this->eventRepository->getSessionListByEventId($inputJson["event_id"]);

            if ($inputJson["flg_file"] == "1") {
                $request->session_file->storeAs('', $inputJson["file_path"], 'public');
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function editSession(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $rules = [
                // 'tenantId' => 'required',
                'event_session_id' => 'required|numeric',
                'session_name' => 'required',
                'session_description' => 'required',
                'flg_file' => 'required|numeric',
                'ticket_list' => 'required',
            ];
            $validator = Validator::make($inputJson, $rules, [
                'required' => 'Kolom :attribute harus diisi.',
            ]);
            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            if ($inputJson["flg_file"] == "1") {
                $filename_uuid = (string) Str::orderedUuid();
                $extension = $request->session_file->extension();
                $mime = $request->session_file->getClientMimeType();

                $inputJson["session_file"] = $request->session_file;
                // var_dump(SPEAKER_PHOTO_PATH . "/" . $filename_uuid . "." . $extension);
                $inputJson["file_path"] = SESSION_PATH . "/" . $filename_uuid . "." . $extension;

                $inputJson["file_mime"] = $mime;
                $rules['session_file'] = 'file|mimes:docx,pdf,doc|max:5120';

                $validator = Validator::make($inputJson, $rules, [
                    'required' => 'Kolom :attribute harus diisi.',
                ]);
                if ($validator->fails()) {
                    throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
                }
            } else {
                //$inputJson["file_path"] = "";
                //$inputJson["file_mime"] = "";
            }

            $eventSession = $this->eventRepository->findEventSessionById($inputJson["event_session_id"]);

            $eventSession = $this->setUdateUserId($eventSession, Auth::id());
            $eventSession->session_name = $inputJson["session_name"];
            $eventSession->session_description = $inputJson["session_description"];

            if ($inputJson["flg_file"] == "1") {
                if ($eventSession->file_path != EMPTY_STRING && file_exists(storage_path('app/public/' . $eventSession->file_path))) {
                    unlink(storage_path('app/public/' . $eventSession->file_path));
                }

                $eventSession->file_path = $inputJson["file_path"];
                $eventSession->file_mime = $inputJson["file_mime"];
            }

            $sessionTicketList = [];
            foreach ($inputJson["ticket_list"] as $ticket) {
                $sessionTicket["event_ticket_id"] = $ticket;
                $sessionTicket = $this->setCreateUserId($sessionTicket, Auth::id());
                $sessionTicketList[] = $sessionTicket;
            }
            $inputJson["sessionTicketList"] = $sessionTicketList;

            $this->eventRepository->editSession($eventSession, $sessionTicketList);
            $resultList = $this->eventRepository->getSessionListByEventId($eventSession->event_id);
            if ($inputJson["flg_file"] == "1") {

                $request->session_file->storeAs('', $inputJson["file_path"], 'public');
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function findEventById(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_id' => 'required|numeric',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $event = $this->eventRepository->findEventById($inputJson['event_id']);
            $speakerList = $this->eventRepository->getSpeakerListByEventId($inputJson['event_id']);
            foreach ($speakerList as $speaker) {
                if ($speaker->photo_path != EMPTY_STRING) {
                    $speaker->photo_url = $this->getUrl("/getSpeakerPhoto/" . $speaker->event_speaker_id);
                }
            }

            $sessionList = $this->eventRepository->getSessionListByEventId($inputJson['event_id']);
            foreach ($sessionList as $session) {
                $session->file_url = "";
                if (EMPTY_STRING != $session->file_path) {
                    $session->file_url = $this->getUrl('api/getSessionFile/' . $session->event_session_id);
                }
            }

            $result = [
                "banner_url" => $this->getUrl("getBannerEvent/" . $event->event_code),
                "event_code" => $event->event_code,
                "event_name" => $event->event_name,
                "event_category" => $event->event_category,
                "event_description" => $event->event_description,
                "date_from" => date('d-m-Y', strtotime($event->date_from)),
                "date_to" => date('d-m-Y', strtotime($event->date_to)),
                "time_from" => date('h:i', strtotime($event->time_from)),
                "time_to" => date('h:i', strtotime($event->time_to)),
                'speakerList' => $speakerList,
                'ticketList' => $this->eventRepository->getTicketListByEventId($inputJson['event_id']),
                'locationList' => $this->eventRepository->getLocationListByEventId($inputJson['event_id']),
                'sessionList' => $sessionList,
            ];
            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function findEventSpeakerById(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_speaker_id' => 'required|numeric',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $result = $this->eventRepository->findEventSpeakerById($inputJson['event_speaker_id']);

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function findEventTicketById(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_ticket_id' => 'required|numeric',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $result = $this->eventRepository->findEventTicketById($inputJson['event_ticket_id']);
            $result->date_from = date("d-m-Y", strtotime($result->date_from));
            $result->date_to = date("d-m-Y", strtotime($result->date_to));
            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function findEventLocationById(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_location_id' => 'required|numeric',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $result = $this->eventRepository->findEventLocationById($inputJson['event_location_id']);
            $result->location_type = $result->location_type . "";
            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function findEventSessionById(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_session_id' => 'required|numeric',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $result = $this->eventRepository->findEventSessionById($inputJson['event_session_id']);
            $eventSessionTicketList = $this->eventRepository->getEventSessionTicketBySessionId($inputJson['event_session_id']);
            $ticketList = [];
            foreach ($eventSessionTicketList as $sessionTicket) {
                $ticketList[] = $sessionTicket->event_ticket_id;
            }
            $result->ticket_list = $ticketList;

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    // public function orderTicket(Request $request)
    // {

    // }

    public function uploadPayment(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $inputJson['document'] = $request->document;
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_ticket_order_id' => 'required',
                'document' => 'required|file|mimes:jpeg,png,jpg,pdf|max:512',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $user = Auth::user();
            $dateTimeNow = $this->getDatetimeNow();
            $dateNow = $this->getDateNow();

            $ticketOrder = $this->eventRepository->findEventTicketOrderById($inputJson['event_ticket_order_id']);

            if ($ticketOrder->order_status_code != ORDER_STATUS_WAITING_PAYMENT_CODE) {
                throw new CoreException(TICKET_IS_BEING_PROCESS, ["0", $ticketOrder->order_code]);
            }

            $filename_uuid = (string) Str::orderedUuid();
            $extension = $request->document->extension();
            $mime = $request->document->getClientMimeType();

            $ticketOrder->document_path = PAYMENT_DOCUMENT_PATH . '/' . $filename_uuid . '.' . $extension;
            $ticketOrder->document_mime = $mime;
            $ticketOrder->order_status = ORDER_STATUS_WAITING_VERIFIED;
            $ticketOrder->order_status_code = ORDER_STATUS_WAITING_VERIFIED_CODE;
            $ticketOrder = $this->setUdateUserId($ticketOrder, $user->user_id);
            $result = $this->eventRepository->editEventTicketOrder($ticketOrder);
            $request->document->storeAs('', $ticketOrder->document_path, 'public');
            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function verifyPayment(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_ticket_order_id' => 'required',
                'remark' => 'present',
                'action' => ['required', Rule::in([ORDER_STATUS_WAITING_PAYMENT_CODE, ORDER_STATUS_VERIFIED_CODE, ORDER_STATUS_REJECTED_CODE])],
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $user = Auth::user();
            $dateTimeNow = $this->getDatetimeNow();
            $dateNow = $this->getDateNow();

            $ticketOrder = $this->eventRepository->findEventTicketOrderById($inputJson['event_ticket_order_id']);

            if ($ticketOrder->order_status_code != ORDER_STATUS_WAITING_VERIFIED_CODE) {
                throw new CoreException(TICKET_ALREADY_VERIFIED, ["0", $ticketOrder->order_code]);
            }
            if (ORDER_STATUS_WAITING_PAYMENT_CODE == $inputJson["action"]) {
                $ticketOrder->order_status = ORDER_STATUS_WAITING_PAYMENT;
            }

            if (ORDER_STATUS_VERIFIED_CODE == $inputJson["action"]) {
                $ticketOrder->order_status = ORDER_STATUS_VERIFIED;
            }

            if (ORDER_STATUS_REJECTED_CODE == $inputJson["action"]) {
                $ticketOrder->order_status = ORDER_STATUS_REJECTED;
            }

            $ticketOrder->order_status_code = $inputJson["action"];
            $ticketOrder->remark = null == $inputJson["remark"] ? "" : $inputJson["remark"];
            $ticketOrder = $this->setUdateUserId($ticketOrder, $user->user_id);
            $result = $this->eventRepository->editEventTicketOrder($ticketOrder);
            if ($result->order_status_code == ORDER_STATUS_VERIFIED_CODE) {
                $getLocationListByEventId = GetLocationListByEventId::getInstance();

                $locationList = $getLocationListByEventId->execute($result->event_id);
                $locationArray = [];
                foreach ($locationList as $location) {
                    $loc = ["location_name" => $location->location_name,
                        "location_url" => $location->location_url,
                        "location_type" => $location->location_type,
                        "location_description" => $location->location_description,
                    ];
                    $locationArray[] = $loc;
                }
                event(new OrderTicketEvent(["locations" => $locationArray, "email" => $result->email]));

            }

            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function getEventByAdvance(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'keyword' => 'present',
                'limit' => 'required',
                'offset' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if (is_null($inputJson['keyword'])) {
                $inputJson['keyword'] = "";
            }

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $resultList = $this->eventRepository->getEventByAdvance($inputJson['keyword'], $inputJson['limit'], $inputJson['offset'], $dateTimeNow);
            foreach ($resultList as $result) {
                $result->date_from = date('d-m-Y', strtotime($result->date_from));
                $result->date_to = date('d-m-Y', strtotime($result->date_to));
                $result->event_description = substr(strip_tags($result->event_description), 0, 50);
                $result->banner_url = $this->getUrl("getBannerEvent/" . $result->event_code);
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function getDetailEvent(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_code' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $event = $this->eventRepository->findEventByCode($inputJson['event_code']);
            $locationList = $this->eventRepository->getLocationListByEventId($event->event_id);
            $findComboConstantItemById = FindComboConstantItemById::getInstance();
            $comboConstant = $findComboConstantItemById->execute($event->event_category);
            $location_type = "";
            foreach ($locationList as $location) {
                if ($location->location_type == '0') {
                    if ($location_type == 'ONLINE') {
                        $location_type = 'ONLINE AND OFFLINE';
                    }
                    if ($location_type == '') {
                        $location_type = 'OFFLINE';
                    }

                }
                if ($location->location_type == '1') {
                    if ($location_type == 'OFFLINE') {
                        $location_type = 'ONLINE AND OFFLINE';
                    }
                    if ($location_type == '') {
                        $location_type = 'ONLINE';
                    }

                }
            }

            $result = [
                "event_category" => $comboConstant->combo_constant_item_name,
                "event_code" => $event->event_code,
                "event_name" => $event->event_name,
                "event_description" => $event->event_description,
                "date_from" => date('d-m-Y', strtotime($event->date_from)),
                "date_to" => date('d-m-Y', strtotime($event->date_to)),
                "time_from" => $event->time_from,
                "time_to" => $event->time_from,
                "banner_url" => $this->getUrl("getBannerEvent/" . $event->event_code),
                'location_type' => $location_type,
            ];
            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function getTicketListForBuy(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_code' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $event = $this->eventRepository->findEventByCode($inputJson['event_code']);
            $result = $this->eventRepository->getTicketListForBuy($event->event_id, $this->getDateNow());
            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function getTicketListForCombo(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_id' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $event = $this->eventRepository->findEventById($inputJson['event_id']);
            $result = $this->eventRepository->getTicketListForCombo($event->event_id);
            return ResponseJson::success($result);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function countGetEventByAdvance(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'keyword' => 'present',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();
            $result = $this->eventRepository->countGetEventByAdvance($inputJson['keyword'], $dateTimeNow);
            return ResponseJson::success($result);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }

    public function getMyEvent(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'keyword' => 'present',
                'limit' => 'required',
                'offset' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if (is_null($inputJson['keyword'])) {
                $inputJson['keyword'] = "";
            }

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $resultList = $this->eventRepository->getMyEvent(Auth::id(), $inputJson['keyword'], $inputJson['limit'], $inputJson['offset'], $dateTimeNow);
            foreach ($resultList as $result) {
                $eventDate = DB::table('event_date')
                    ->where('event_id', $result->event_id)
                    ->first();
                $result->date_from = date('d-m-Y', strtotime($eventDate->event_date));
                $result->date_to = date('d-m-Y', strtotime($eventDate->event_date));
                $result->time_from = $eventDate->time_from;
                $result->time_to = $eventDate->time_to;

                $result->active = $result->active == 'Y' ? 'YES' : 'NO';
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
    public function countGetMyEvent(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'keyword' => 'present',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();
            $result = $this->eventRepository->countGetMyEvent(Auth::id(), $inputJson['keyword'], $dateTimeNow);
            return ResponseJson::success($result);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }

    public function getTicketSales(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_code' => 'required',
                'keyword' => 'present',
                'limit' => 'required',
                'offset' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if (is_null($inputJson['keyword'])) {
                $inputJson['keyword'] = "";
            }

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $resultList = $this->eventRepository->getTicketSales($inputJson['event_code'], $inputJson['keyword'], $inputJson['limit'], $inputJson['offset'], $dateTimeNow);
            foreach ($resultList as $result) {
                $result->is_pdf = $result->document_mime == "application/pdf" ? true : false;
                $result->url = $this->getUrl("getDocumentOrder/" . $result->event_ticket_order_id);
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }
    public function countGetTicketSales(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'event_code' => 'required',
                'keyword' => 'present',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();
            $result = $this->eventRepository->countGetTicketSales($inputJson['event_code'], $inputJson['keyword'], $dateTimeNow);
            return ResponseJson::success($result);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }

    public function getMyTicket(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }

            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'keyword' => 'present',
                'limit' => 'required',
                'offset' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if (is_null($inputJson['keyword'])) {
                $inputJson['keyword'] = "";
            }

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();

            $resultList = $this->eventRepository->getMyTicket(Auth::id(), $inputJson['keyword'], $inputJson['limit'], $inputJson['offset'], $dateTimeNow);
            foreach ($resultList as $result) {
                $result->date_from = date('d-m-Y', strtotime($result->date_from));
                $result->date_to = date('d-m-Y', strtotime($result->date_to));
            }

            return ResponseJson::success($resultList);
        } catch (Exception $e) {
            Log::error($e);
            return ResponseJson::fail($e);
        }
    }

    public function countGetMyTicket(Request $request)
    {
        try {
            if ($this->isJson($request->getContent())) {
                $inputJson = json_decode($request->getContent(), true);
            } else {
                $inputJson = $request->input();
            }
            $validator = Validator::make($inputJson, [
                // 'tenantId' => 'required',
                'keyword' => 'present',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ]);

            if ($validator->fails()) {
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(), []);
            }

            $dateTimeNow = $this->getDatetimeNow();
            $result = $this->eventRepository->countGetMyTicket(Auth::id(), $inputJson['keyword'], $dateTimeNow);
            return ResponseJson::success($result);
        } catch (Exception $e) {
            return ResponseJson::fail($e);
        }
    }
}
