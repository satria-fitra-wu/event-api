<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function setActive($data)
    {
        if (is_array($data)) {
            $data['active'] = FLAG_YES;
        } else {
            $data->active = FLAG_YES;
        }
        return $data;
    }

    public function setNonActive($data)
    {
        if (is_array($data)) {
            $data['active'] = FLAG_NO;
        } else {
            $data->active = FLAG_NO;
        }
        return $data;
    }

    public function setCreateUserId($data, $userId)
    {
        if (is_array($data)) {
            $data['create_user_id'] = $userId;
            $data['update_user_id'] = $userId;
        } else {
            $data->create_user_id = $userId;
            $data->update_user_id = $userId;
        }
        return $data;
    }

    public function setUdateUserId($data, $userId)
    {
        if (is_array($data)) {
            $data['update_user_id'] = $userId;
        } else {
            $data->update_user_id = $userId;
        }
        return $data;
    }

    public function getDatetimeNow()
    {
        return date("Y-m-d H:i:s");
    }

    public function getDateNow()
    {
        return date("Y-m-d");
    }

    public function getUrl($path)
    {
        if (app('env') == 'production') {
            //
            return secure_url($path);
        } else {
            return url($path);

        }
    }
}
