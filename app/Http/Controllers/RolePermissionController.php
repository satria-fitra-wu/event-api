<?php

namespace App\Http\Controllers;

use App\Exceptions\CoreException as CoreException;
use App\Http\Helper\ResponseJson;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repository\RolePermissionRepository;

class RolePermissionController extends Controller
{
    public $rolePermissionRepository;

    public function __construct(RolePermissionRepository $rolePermissionRepository)
    {
        $this->rolePermissionRepository = $rolePermissionRepository::getInstance();
    }
    //
    
    public function addRole(Request $request){
        try {
            if($this ->isJson($request->getContent())){
                $inputJson = json_decode($request->getContent(),true); 
            }else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'roleName' => 'required',
            ]);
            
            if ($validator ->fails()){
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(),[]);
            }

            $resultIsExists = $this -> rolePermissionRepository -> isRoleExistByRoleName($request);

            if(!$resultIsExists->exists) {
                $result = $this -> rolePermissionRepository -> addRole(["name" => $inputJson["roleName"]]);
                return ResponseJson::success($result);
            }else{
                throw new CoreException(DATA_ALREADY_EXISTS,["0" => "role"]);
            }
        } catch (Exception $e){
            return ResponseJson::fail($e);
        }
    }

    public function deleteRole(Request $request){
        try {
            if($this ->isJson($request->getContent())){
                $inputJson = json_decode($request->getContent(),true); 
            }else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'roleId' => 'required',
            ]);
            
            if ($validator ->fails()){
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(),[]);
            }

            $role = $this -> rolePermissionRepository -> findRoleById($inputJson["roleId"]);

            $this -> rolePermissionRepository -> deleteRole($role);

            return ResponseJson::success(["msg" =>SUCCESS_DELETE_DATA]);
        } catch (Exception $e){
            return ResponseJson::fail($e);
        }
    }

    public function addPermission(Request $request){
        try {
            if($this ->isJson($request->getContent())){
                $inputJson = json_decode($request->getContent(),true); 
            }else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'permissionName' => 'required',
            ]);
            
            if ($validator ->fails()){
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(),[]);
            }

            $resultIsExists = $this -> rolePermissionRepository -> isPermissionExistByRoleName($request);

            if(!$resultIsExists->exists) {
                $result = $this -> rolePermissionRepository -> addPermission(["name" => $inputJson["permissionName"]]);
                return ResponseJson::success($result);
            }else{
                throw new CoreException(DATA_ALREADY_EXISTS,["0" => "permission"]);
            }
        } catch (Exception $e){
            return ResponseJson::fail($e);
        }
    }

    public function deletePermission(Request $request){
        try {
            if($this ->isJson($request->getContent())){
                $inputJson = json_decode($request->getContent(),true); 
            }else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'permissionId' => 'required',
            ]);
            
            if ($validator ->fails()){
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(),[]);
            }

            $permission = $this -> rolePermissionRepository -> findPermissionById($inputJson["permissionId"]);

            $this -> rolePermissionRepository -> deletePermission($permission);
            return ResponseJson::success(["msg" =>SUCCESS_DELETE_DATA]);
        } catch (Exception $e){
            return ResponseJson::fail($e);
        }
    }

    public function addPermissionToRole(Request $request){
        try {
            if($this ->isJson($request->getContent())){
                $inputJson = json_decode($request->getContent(),true); 
            }else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'roleId' => 'required',
                'permissionId' => 'required',
            ]);
            
            if ($validator ->fails()){
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(),[]);
            }
            $role = $this -> rolePermissionRepository -> findRoleById($inputJson["roleId"]);
            $permission = $this -> rolePermissionRepository -> findPermissionById($inputJson["permissionId"]);

            $role -> givePermissionTo($permission);

            return ResponseJson::success($permission);
        } catch (Exception $e){
            return ResponseJson::fail($e);
        }
    }

    public function deletePermissionFromRole(Request $request){
        try {
            if($this ->isJson($request->getContent())){
                $inputJson = json_decode($request->getContent(),true); 
            }else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'roleId' => 'required',
                'permissionId' => 'required',
            ]);
            
            if ($validator ->fails()){
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(),[]);
            }
            $role = $this -> rolePermissionRepository -> findRoleById($inputJson["roleId"]);
            $permission = $this -> rolePermissionRepository -> findPermissionById($inputJson["permissionId"]);

            $role -> revokePermissionTo($permission);
            
            return ResponseJson::success(["msg" =>SUCCESS_DELETE_DATA]);
        } catch (Exception $e){
            return ResponseJson::fail($e);
        }
    }

    public function getRole(Request $request){
        try {
            if($this ->isJson($request->getContent())){
                $inputJson = json_decode($request->getContent(),true); 
            }else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'roleName' => 'present',
            ]);
            
            if ($validator ->fails()){
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(),[]);
            }
            $roleList = $this -> rolePermissionRepository -> getRole($inputJson["roleName"]);
            
            return ResponseJson::success($roleList);
        } catch (Exception $e){
            return ResponseJson::fail($e);
        }
    }

    public function getPermission(Request $request){
        try {
            if($this ->isJson($request->getContent())){
                $inputJson = json_decode($request->getContent(),true); 
            }else {
                $inputJson = $request->input();
            }
            //validasi input
            $validator = Validator::make($inputJson, [
                'permissionName' => 'present',
            ]);
            
            if ($validator ->fails()){
                throw new CoreException(ERROR_DATA_VALIDATION, $validator->getMessageBag(),[]);
            }
            $permissionList = $this -> rolePermissionRepository -> getRole($inputJson["permissionName"]);
            
            return ResponseJson::success($permissionList);
        } catch (Exception $e){
            return ResponseJson::fail($e);
        }
    }
}
