<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventSpeaker extends Model
{
    use HasFactory;
    protected $table = "event_speaker";
    protected $primaryKey = 'event_speaker_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'line_no', 'speaker_name', 'speaker_description', 'banner_path', 'banner_mime',
        'create_user_id', 'update_user_id', 'active'
    ];
}
