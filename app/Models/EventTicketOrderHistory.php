<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventTicketOrderHistory extends Model
{
    use HasFactory;
    protected $table = "event_ticket_order_history";
    protected $primaryKey = 'id';
    protected $fillable = [
        'event_ticket_order_id','event_id', 'event_ticket_id', 'user_id_as_buyer', 'order_code',
        'line_no', 'order_user_id', 'name',
        'email', 'instance', 'phone', 'address',
        'payment_method', 'order_status', 'order_status_code',
        'document_path', 'document_mime', 'order_price',
    ];
}
