<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $table = "event";
    protected $primaryKey = 'event_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id', 'event_code', 'event_name', 'event_category', 'date_from', 'date_to', 'time_from', 'time_to',
        'event_description', 'banner_path', 'banner_mime', 'organizer_user_id',
        'create_user_id', 'update_user_id', 'active', 'course_url',
    ];
}
