<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComboConstantItem extends Model
{
    use HasFactory;
    protected $table = "combo_constant_item";
    protected $primaryKey = 'combo_constant_item_id';

    public $incrementing = true;

    protected $fillable = [
        'combo_constant_id',
        'combo_constant_item_code',
        'combo_constant_item_name',
        'system',
        'active',
        'create_user_id',
        'update_user_id',
        'version',
    ];
}
