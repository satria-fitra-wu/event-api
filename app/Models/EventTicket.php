<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventTicket extends Model
{
    use HasFactory;

    protected $table = "event_ticket";
    protected $primaryKey = 'event_ticket_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'ticket_name', 'ticket_price', 'ticket_description',
        'create_user_id', 'update_user_id', 'active', 'quota', 'line_no', 'date_from', 'date_to',
    ];
}
