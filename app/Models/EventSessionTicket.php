<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventSessionTicket extends Model
{
    use HasFactory;
    protected $table = "event_session_ticket";
    protected $primaryKey = 'event_session_ticket_id';

    protected $fillable = [
        'event_session_id', 'event_ticket_id',
        'create_user_id', 'update_user_id'
    ];
}
