<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventCertificateParameter extends Model
{
    use HasFactory;
    protected $table = "event_certificate_parameter";
    protected $primaryKey = 'event_certificate_parameter_id';
    protected $guarded = [
        'event_certificate_parameter_id',
        'created_at',
        'updated_at'
    ];
}
