<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventSession extends Model
{
    use HasFactory;
    protected $table = "event_session";
    protected $primaryKey = 'event_session_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'event_ticket_id', 'line_no', 'session_name', 'session_description', 'file_path', 'file_mime',
        'create_user_id', 'update_user_id', 'active',
    ];
}
