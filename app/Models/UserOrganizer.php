<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOrganizer extends Model
{
    use HasFactory;
    protected $table = "user_organizer";
    protected $primaryKey = 'user_id';
    protected $guarded = ["user_id"];
}
