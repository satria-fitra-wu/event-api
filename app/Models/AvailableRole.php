<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvailableRole extends Model
{
    use HasFactory;

    protected $table = 'available_role';

    protected $primaryKey = 'available_role_id';

    protected $fillable = [
        'user_id',
        'role_id',
        'create_user_id',
        'update_user_id',
    ];
}
