<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerCarousel extends Model
{
    use HasFactory;
    protected $table = "banner_carousel";
    protected $primaryKey = 'banner_carousel_id';
    protected $guarded = ["banner_carousel_id"];
}
