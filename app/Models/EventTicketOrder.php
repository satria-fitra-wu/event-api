<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventTicketOrder extends Model
{
    use HasFactory;
    protected $table = "event_ticket_order";
    protected $primaryKey = 'event_ticket_order_id';
    protected $fillable = [
        'event_id', 'event_ticket_id', 'user_id_as_buyer', 'order_code',
        'line_no', 'order_user_id', 'name',
        'email', 'instance', 'phone', 'address',
        'payment_method', 'order_status', 'order_status_code', 'remark',
        'document_path', 'document_mime', 'order_price', 'create_user_id', 'update_user_id',
    ];
}
