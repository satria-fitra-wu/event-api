<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventLocation extends Model
{
    use HasFactory;
    protected $table = "event_location";
    protected $primaryKey = 'event_location_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'line_no', 'location_name', 'location_type', 'location_url', 'location_description', 'latitude',
        'longitude',
        'create_user_id', 'update_user_id', 'active'
    ];
}
