<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComboConstant extends Model
{
    use HasFactory;
    protected $table = "combo_constant";
    protected $primaryKey = 'combo_constant_id';

    public $incrementing = true;

    protected $fillable = [
        'flg_system',
        'combo_constant_code',
        'combo_constant_name',
        'active',
        'create_user_id',
        'update_user_id',
        'version',
    ];
}
