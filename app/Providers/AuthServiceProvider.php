<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Route;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Passport::tokensExpireIn(now()->addDays(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));
		Route::prefix('api')->group(function () {
            Passport::routes();
        });
        //Passport::hashClientSecrets();
        //Passport::personalAccessTokensExpireIn(now()->addMonths(6));
        //if (! $this->app->routesAreCached()) {
        //    
        //}

        //
        //setting untuk Authorize
        //Gate::before(function($user, $ability){
        //    return $user->hasRole("SUPERADMIN") ? true : null;
        //});
    }
}
