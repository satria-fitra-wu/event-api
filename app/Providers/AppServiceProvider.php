<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        require_once dirname(__DIR__) . "/Constant/SystemConstant.php";
        require_once dirname(__DIR__) . "/Constant/ErrorConstant.php";
        require_once dirname(__DIR__) . "/Constant/MessageConstant.php";
    }
}
