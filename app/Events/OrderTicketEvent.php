<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderTicketEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    public function __construct(array $data)
    {
        //
        $this->data = $data;
    }
}
