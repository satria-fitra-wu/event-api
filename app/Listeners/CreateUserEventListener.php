<?php

namespace App\Listeners;

use App\Events\CreateUserEvent;
use App\Mail\SendUsernamePassword;
use Illuminate\Support\Facades\Mail;

class CreateUserEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CreateUserEvent  $event
     * @return void
     */
    public function handle(CreateUserEvent $event)
    {
        //
        Mail::to($event->user["email"])->send(new SendUsernamePassword($event->user));
    }
}
