<?php

namespace App\Listeners;

use App\Events\OrderTicketEvent;
use App\Mail\SendDetailLocationEvent;
use Illuminate\Support\Facades\Mail;

class OrderTicketEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderTicketEvent  $event
     * @return void
     */
    public function handle(OrderTicketEvent $event)
    {
        //
        Mail::to($event->data["email"])->send(new SendDetailLocationEvent($event->data));

    }
}
