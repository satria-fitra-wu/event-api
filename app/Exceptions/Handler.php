<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
//use \Spatie\Permission\Exceptions\UnauthorizedException

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }


    protected function convertExceptionToArray(Throwable $e)
    {
        //\Spatie\Permission\Exceptions\UnauthorizedException
        if ($e instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return [
                "errorKey" => 403,
                "success" => false,
                "data" => [
                    "msg" => $e->getMessage()
                ],
            ];
        }

        return config('app.debug') ? [
            "success" => false,
            "data" => [
                'msg' => get_class($e) . ' ' . $e->getMessage(),
                'exception' => get_class($e),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => collect($e->getTrace())->map(function ($trace) {
                    return Arr::except($trace, ['args']);
                })->all(),
            ]
        ] : [
            "success" => false,
            "data" => [
                "msg" => $this->isHttpException($e) ? $e->getMessage() : 'Server Error'
            ],
        ];
    }
    /*
    protected function prepareJsonResponse($request, Throwable $e)
    {
        return new JsonResponse(
            $this->convertExceptionToArray($e),
            $this->isHttpException($e) ? $e->getStatusCode() : 500,
            $this->isHttpException($e) ? $e->getHeaders() : [],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
        );
    }*/

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            $jsonMsg = [
                "msg" => $exception->getMessage(),
                "errorKey" => 401
            ];
            $json = [
                'success' => false,
                "msg" => $exception->getMessage(),
                "errorKey" => 401,
                'data' => $jsonMsg
            ];
            return response()
                ->json($json, 401);
        }
        /*$guard = array_get($exception->guards(),0);
        switch ($guard) {
            default:
                $login = 'login';
                break;
        }
        return redirect()->guest(route($login));*/
    }
}
