<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;

class MakeRepositoryCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //protected $signature = 'make:repository {name}';

    protected $name = 'make:repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Repository';

    protected $type = 'class';

    protected function getStub() {
        return resource_path('stubs/repository.stub');

     }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Repository';
    }

}
