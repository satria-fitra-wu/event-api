<?php

define("ADD_CUTI_FROM_MOBILE", "Add cuti dari mobile");
define("ADD_DINAS_LUAR_FROM_MOBILE", "Add dinas luar dari mobile");
define("CHECKIN_FROM_MOBILE", "Absen masuk dari mobile");
define("CHECKOUT_FROM_MOBILE", "Absen pulang dari mobile");
define("EMPTY_STRING", "");
define("ERROR_DATA_VALIDATION", "ERROR_DATA_VALIDATION");
define("EVENT_BANNER_PATH", "event_banner");
define("ID_MESIN_ABSENSI", 55);
define("NULL_REF_INT", -99);
define("ORDER_STATUS_WAITING_PAYMENT", "WAITING PAYMENT");
define("ORDER_STATUS_WAITING_PAYMENT_CODE", "00");
define("ORDER_STATUS_WAITING_VERIFIED", "WAITING VERIFY");
define("ORDER_STATUS_WAITING_VERIFIED_CODE", "10");
define("ORDER_STATUS_VERIFIED", "VERIFIED");
define("ORDER_STATUS_VERIFIED_CODE", "20");
define("ORDER_STATUS_REJECTED", "REJECTED");
define("ORDER_STATUS_REJECTED_CODE", "30");
define("PAYMENT_DOCUMENT_PATH", "payment_document");
define("PAYMENT_METHOD_MANUAL", "MANUAL");
define("ROOT_DISK", "root");
define("SPEAKER_PHOTO_PATH", "speaker_photo");
define("SESSION_PATH", "session_file");
define("TEMPLATE_CERTIFICATE_PATH", "template_certificate");

define("YES", "YES");
define("FLAG_YES", "Y");
define("FLAG_NO", "N");
define("ZERO", "0");
define("BANNER_CAROUSEL_PATH", "banner_carousel");
