<?php

use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\DownloadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/getBannerEvent/{code}', [DownloadController::class, "getBannerEvent"])->name("getBannerEvent");
Route::get('/getDocumentOrder/{id}', [DownloadController::class, "getDocumentOrder"])->name("getDocumentOrder");
Route::get('/getSpeakerPhoto/{id}', [DownloadController::class, "getSpeakerPhoto"])->name("getSpeakerPhoto");
Route::get('/reset-password/{token}', function ($token) {
    return view('auth.reset-password', ['token' => $token]);
})->middleware('guest')->name('password.reset');

Route::get('verify-email/{id}/{hash}', VerifyEmailController::class)->name('verification.verify')
    ->middleware('signed', 'throttle:6,1');

Route::get('{path}', function () {
    return view('welcome');
})->where('path', '.*');

// Route::get('/', function () {
//     $query = http_build_query([
//         'client_id' => '932ac885-9b08-4ea5-84f9-754e41044127', // Replace with Client ID
//         'redirect_uri' => 'http://127.0.0.1:8000/callback',
//         'response_type' => 'code',
//         'scope' => '',
//     ]);
//     //var_dump($response);
//     return redirect('http://127.0.0.1:8000/oauth/authorize?' . $query);
// });
// Route::get('/hasil', function (Request $request) {
//     var_dump($request->input());
// });

// Route::get('/callback', function (Request $request) {
//     var_dump($request->input());
//     $response = (new GuzzleHttp\Client)->post('http://127.0.0.1:8000/oauth/token', [
//         'form_params' => [
//             'grant_type' => 'authorization_code',
//             'client_id' => '932ac885-9b08-4ea5-84f9-754e41044127', // Replace with Client ID
//             'client_secret' => 'aroCzTQLRjyEnbBXT5EmG2YcwRSwkjqPMQrDZ0qi', // Replace with client secret
//             'redirect_uri' => 'http://127.0.0.1:8000/hasil',
//             'code' => $request->code,
//         ],
//     ]);
//     //var_dump($response);
//     //session()->put('token', json_decode((string) $response->getBody(), true));

//     //return redirect('/todos');
// });

// Route::get('/todos', function () {
//     $response = (new GuzzleHttp\Client)->get('http://127.0.0.1:8000/todos', [
//         'headers' => [
//             'Authorization' => 'Bearer ' . session()->get('token.access_token'),
//         ],
//     ]);

//     return json_decode((string) $response->getBody(), true);
// });
