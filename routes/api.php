<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Auth\SendVerificationEmailController;
use App\Http\Controllers\BannerCarousel\AddBannerCarouselController;
use App\Http\Controllers\BannerCarousel\CountGetBannerCarouselListController;
use App\Http\Controllers\BannerCarousel\DeleteBannerCarouselController;
use App\Http\Controllers\BannerCarousel\DownloadBannerCarouselImageController;
use App\Http\Controllers\BannerCarousel\EditBannerCarouselController;
use App\Http\Controllers\BannerCarousel\FindBannerCarouselByIdController;
use App\Http\Controllers\BannerCarousel\GetBannerCarouselListController;
use App\Http\Controllers\ComboConstantController;
use App\Http\Controllers\DownloadController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\Event\AddCertificateParameterController;
use App\Http\Controllers\Event\AddEventController;
use App\Http\Controllers\Event\AddEventDateController;
use App\Http\Controllers\Event\AddEventLocationController;
use App\Http\Controllers\Event\AddEventSpeakerController;
use App\Http\Controllers\Event\AddEventTicketController;
use App\Http\Controllers\Event\CountGetEventByFilterListController;
use App\Http\Controllers\Event\DeleteCertificateParameterController;
use App\Http\Controllers\Event\DeleteEventDateController;
use App\Http\Controllers\Event\DeleteEventLocationController;
use App\Http\Controllers\Event\DeleteEventSpeakerController;
use App\Http\Controllers\Event\DeleteEventTicketController;
use App\Http\Controllers\Event\DownloadCertificateEventController;
use App\Http\Controllers\Event\DownloadCertificateEventForUserController;
use App\Http\Controllers\Event\EditCertificateParameterController;
use App\Http\Controllers\Event\EditEventActiveStatusController;
use App\Http\Controllers\Event\EditEventController;
use App\Http\Controllers\Event\EditEventDateController;
use App\Http\Controllers\Event\EditEventLocationController;
use App\Http\Controllers\Event\EditEventSpeakerController;
use App\Http\Controllers\Event\EditEventTicketController;
use App\Http\Controllers\Event\FindEventByIdController;
use App\Http\Controllers\Event\FindEventCertificateParameterByIdController;
use App\Http\Controllers\Event\FindEventDateByIdController;
use App\Http\Controllers\Event\FindEventLocationByIdController;
use App\Http\Controllers\Event\FindEventTicketByIdController;
use App\Http\Controllers\Event\GetDetailEventController;
use App\Http\Controllers\Event\GetEventByFilterListController;
use App\Http\Controllers\Event\GetEventLocationListByEventTicketOrderIdController;
use App\Http\Controllers\Event\OrderTicketController;
use App\Http\Controllers\Event\UploadTemplateCertificateController;
use App\Http\Controllers\Home\GetFeaturedEventController;
use App\Http\Controllers\Home\GetTopEventController;
use App\Http\Controllers\Home\GetTopSellingEventController;
use App\Http\Controllers\RolePermissionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserOrganizer\AddUserOrganizerController;
use App\Http\Controllers\UserOrganizer\EditProfileController;
use App\Http\Controllers\UserOrganizer\GetProfileByUserIdController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!ad
|
 */

//event
Route::post('/getDetailEvent', GetDetailEventController::class)->name("event.getDetailEvent");
Route::post('/getEventByAdvance', [EventController::class, "getEventByAdvance"])->name("event.getEventByAdvance");
Route::post('/countGetEventByAdvance', [EventController::class, "countGetEventByAdvance"])->name("event.countGetEventByAdvance");
Route::post('/getFeaturedEvent', GetFeaturedEventController::class);
Route::post('/getTopEvent', GetTopEventController::class);
Route::post('/getTopSellingEvent', GetTopSellingEventController::class);
Route::post('/getComboConstantItemListForCombo', [ComboConstantController::class, "getComboConstantItemListForCombo"])->name("combo.getComboConstantItemListForCombo");

Route::post('/getEventByFilterList', GetEventByFilterListController::class);
Route::post('/countGetEventByFilterList', CountGetEventByFilterListController::class);
Route::get("/bannerCarousel/{id}/{file_name}", DownloadBannerCarouselImageController::class);

//Authentication
Route::post('/login', [AuthController::class, "login"])->name("auth.login");
Route::post('/register', [UserController::class, "addUser"])->name("user.addUser");

Route::group(['middleware' => 'auth:api'], function () {

    //user organizer
    Route::post("/addUserOrganizer", AddUserOrganizerController::class);
    Route::post("/getProfile", GetProfileByUserIdController::class);
    Route::post("/editProfile", EditProfileController::class);

    //auth
    Route::post('/sendVerificationEmail', SendVerificationEmailController::class);
    Route::post('/logout', [AuthController::class, "logout"])->name("auth.logout");
    Route::post('/isTokenValid', [AuthController::class, "isTokenValid"])->name("auth.isLoggedIn");
    //combo constant

    //banner carousel
    Route::post('/addBannerCarousel', AddBannerCarouselController::class);
    Route::post('/editBannerCarousel', EditBannerCarouselController::class);
    Route::post('/deleteBannerCarousel', DeleteBannerCarouselController::class);
    Route::post('/getBannerCarouselList', GetBannerCarouselListController::class);
    Route::post('/countGetBannerCarouselList', CountGetBannerCarouselListController::class);
    Route::post('/findBannerCarouselById', FindBannerCarouselByIdController::class);

    //event
    Route::get('/downloadCertificate', DownloadCertificateEventController::class)->name("event.downloadCertificate");
    Route::get('/downloadCertificateForUser', DownloadCertificateEventForUserController::class)->name("event.downloadCertificateForUser");

    Route::post('/uploadTemplateCertificate', UploadTemplateCertificateController::class)->name("event.uploadTemplateCertificate");
    Route::post('/addCertificateParameter', AddCertificateParameterController::class)->name("event.addCertificateParameter");
    Route::post('/editCertificateParameter', EditCertificateParameterController::class)->name("event.editCertificateParameter");

    Route::post('/deleteCertificateParameter', DeleteCertificateParameterController::class)->name("event.deleteCertificateParameter");
    Route::post('/findEventCertificateParameterById', FindEventCertificateParameterByIdController::class)->name("event.findEventCertificateParameterById");

    Route::post('/addEvent', AddEventController::class)->name("event.addEvent");
    Route::post('/addEventDate', AddEventDateController::class)->name("event.addEventDate");
    Route::post('/editEventDate', EditEventDateController::class)->name("event.editEventDate");
    Route::post('/deleteEventDate', DeleteEventDateController::class)->name("event.deleteEventDate");

    Route::post('/editEvent', EditEventController::class)->name("event.editEvent");
    Route::post('/editEventActiveStatus', EditEventActiveStatusController::class)->name("event.editEvent");

    Route::post('/addSpeaker', AddEventSpeakerController::class)->name("event.addSpeaker");
    Route::post('/editSpeaker', EditEventSpeakerController::class)->name("event.editSpeaker");
    Route::post('/deleteEventSpeaker', DeleteEventSpeakerController::class)->name("event.deleteSpeaker");

    Route::post('/addTicket', AddEventTicketController::class)->name("event.addTicket");
    Route::post('/editTicket', EditEventTicketController::class)->name("event.editTicket");
    Route::post('/deleteEventTicket', DeleteEventTicketController::class)->name("event.deleteTicket");

    Route::post('/addLocation', AddEventLocationController::class)->name("event.addLocation");
    Route::post('/editLocation', EditEventLocationController::class)->name("event.editLocation");
    Route::post('/deleteEventLocation', DeleteEventLocationController::class)->name("event.deleteLocation");

    Route::post('/addSession', [EventController::class, "addSession"])->name("event.addSession");
    Route::post('/editSession', [EventController::class, "editSession"])->name("event.editSession");

    Route::post('/findEventById', FindEventByIdController::class)->name("event.findEventById");
    Route::post('/findEventDateById', FindEventDateByIdController::class)->name("event.findEventDateById");

    Route::post('/findEventSpeakerById', [EventController::class, "findEventSpeakerById"])->name("event.findEventSpeakerById");
    Route::post('/findEventTicketById', FindEventTicketByIdController::class)->name("event.findEventTicketById");
    Route::post('/findEventLocationById', FindEventLocationByIdController::class)->name("event.findEventLocationById");
    Route::post('/findEventSessionById', [EventController::class, "findEventSessionById"])->name("event.findEventSessionById");

    Route::post('/getTicketListForBuy', [EventController::class, "getTicketListForBuy"])->name("event.getTicketListForBuy");
    Route::post('/getTicketListForCombo', [EventController::class, "getTicketListForCombo"])->name("event.getTicketListForCombo");

    Route::post('/orderTicket', OrderTicketController::class)->name("event.orderTicket");
    Route::post('/uploadPayment', [EventController::class, "uploadPayment"])->name("event.uploadPayment");
    Route::post('/verifyPayment', [EventController::class, "verifyPayment"])->name("event.verifyPayment");
    Route::post('/getMyTicket', [EventController::class, "getMyTicket"])->name("event.getMyTicket");
    Route::post('/countGetMyTicket', [EventController::class, "countGetMyTicket"])->name("event.countGetMyTicket");
    Route::post('/getMyEvent', [EventController::class, "getMyEvent"])->name("event.getMyEvent");
    Route::post('/countGetMyEvent', [EventController::class, "countGetMyEvent"])->name("event.countGetMyEvent");
    Route::post('/getTicketSales', [EventController::class, "getTicketSales"])->name("event.getTicketSales");
    Route::post('/getEventLocationListByEventTicketOrderId', GetEventLocationListByEventTicketOrderIdController::class)->name("event.getEventLocationListByEventTicketOrderId");

    Route::post('/countGetTicketSales', [EventController::class, "countGetTicketSales"])->name("event.countGetTicketSales");
    Route::get('/getSessionFile/{eventSessionId}', [DownloadController::class, "getSessionFile"])->name("event.getSessionFile");

    //role
    Route::post('/addRole', [RolePermissionController::class, "addRole"])
        ->middleware('permission:ADD_ROLE')
        ->name("rolePermission.addRole");

    Route::post('/deleteRole', [RolePermissionController::class, "deleteRole"])
        ->middleware('permission:DELETE_ROLE')
        ->name("rolePermission.deleteRole");

    Route::post('/addPermission', [RolePermissionController::class, "addPermission"])
        ->middleware('permission:ADD_PERMISSION')
        ->name("rolePermission.addPermission");

    Route::post('/deletePermission', [RolePermissionController::class, "deletePermission"])
        ->middleware('permission:DELETE_PERMISSION')
        ->name("rolePermission.deletePermission");

    Route::post('/addPermissionToRole', [RolePermissionController::class, "addPermissionToRole"])
        ->middleware('permission:MANAGE_ROLE')
        ->name("rolePermission.addPermissionToRole");

    Route::post('/deletePermissionFromRole', [RolePermissionController::class, "deletePermissionFromRole"])
        ->middleware('permission:MANAGE_ROLE')
        ->name("rolePermission.deletePermissionFromRole");

    Route::post('/getRole', [RolePermissionController::class, "getRole"])
        ->middleware('permission:VIEW_ROLE')
        ->name("rolePermission.getRole");

    Route::post('/getPermission', [RolePermissionController::class, "getPermission"])
        ->middleware('permission:VIEW_PERMISSION')
        ->name("rolePermission.getPermission");

    //User
    Route::post('/addUser', [RolePermissionController::class, "addUser"])
        ->middleware('permission:ADD_USER')
        ->name("user.addUser");

    Route::post('/addRoleToUser', [RolePermissionController::class, "addRoleToUser"])
        ->middleware('permission:MANAGE_USER')
        ->name("user.addRoleToUser");

    Route::post('/removeRoleFromUser', [RolePermissionController::class, "removeRoleFromUser"])
        ->middleware('permission:MANAGE_USER')
        ->name("user.removeRoleFromUser");

    Route::post('/SetRoleToCurrentUser', [RolePermissionController::class, "SetRoleToCurrentUser"])
        ->middleware('permission:MANAGE_USER')
        ->name("user.SetRoleToCurrentUser");

    Route::post('/asignRole', function () {
        //Permission::create(['name' => 'ADD_ROLE']);
        $permission = Permission::findById(1);
        $role = Role::findById(2);
        $role->givePermissionTo($permission);
        Auth::user()->syncRoles("ADMIN");
    })
        ->name("rolePermission.addRole");
});

/*Route::post('/loginWithCheckAlreadyLoginInOtherDevice', [AuthController::class,"loginWithCheckAlreadyLoginInOtherDevice"])->name("auth.loginWithCheckAlreadyLoginInOtherDevice");

Route::post('/logout', [AuthController::class,"logout"])->name("auth.logout");
Route::post('/register', [AuthController::class,"register"])->name("auth.register");

Route::group(['middleware' => 'auth:api'], function(){
Route::post('/checkIn', [AbsensiController::class,"checkIn"])->name("absensi.checkIn");
Route::post('/checkOut', [AbsensiController::class,"checkOut"])->name("absensi.checkOut");
Route::post('/getRecapAbsenSidak', [AbsensiController::class,"getRecapAbsenSidak"])->name("absensi.getRecapAbsenSidak");
Route::post('/getCurrentStatus', [AbsensiController::class,"getCurrentStatus"])->name("absensi.getCurrentStatus");
Route::post('/getProfile', [AbsensiController::class,"getProfile"])->name("absensi.getProfile");

Route::post('/addCuti', [CutiController::class,"addCuti"])->name("cuti.addCuti");
Route::post('/test', function(Request $request){
return $request->user();
//return "success";
});
});*/
