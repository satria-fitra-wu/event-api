<?php

return [
    'data.not.found' => 'Data :0 not found',
    'date.from.must.be.less.or.equals.with.date.to' => 'Date from (:0) must be less or equals with date to (:1)',
    'username.and.password.not.match' => 'username and password not match',
    'user.already.logout' => 'user already logout'
];
